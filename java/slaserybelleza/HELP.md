##Generate War: ./gradlew war

#Example of the stating of the microservice:
   _____            _                                   ____       _ _               
/ ____|          | |                                 |  _ \     | | |              
| (___    ______  | |     __ _ ___  ___ _ __   _   _  | |_) | ___| | | ___ ______ _
\___ \  |______| | |    / _` / __|/ _ | '__| | | | | |  _ < / _ | | |/ _ |_  / _` |
____) |          | |___| (_| \__ |  __| |    | |_| | | |_) |  __| | |  __// | (_| |
|_____/           |______\__,_|___/\___|_|     \__, | |____/ \___|_|_|\___/___\__,_|
__/ |                               
|___/
:: Spring Boot ::                (v2.5.4)

2021-12-18 13:11:30.430  INFO 25823 --- [  restartedMain] c.s.s.SlaserybellezaApplication          : Starting SlaserybellezaApplication using Java 11.0.11 on GT009870N90578M with PID 25823 (/Users/n90578/Documents/WksJavi/java/slaserybelleza/build/classes/java/main started by n90578 in /Users/n90578/Documents/WksJavi/java/slaserybelleza)
2021-12-18 13:11:30.432  INFO 25823 --- [  restartedMain] c.s.s.SlaserybellezaApplication          : The following profiles are active: local
2021-12-18 13:11:30.464  INFO 25823 --- [  restartedMain] .e.DevToolsPropertyDefaultsPostProcessor : Devtools property defaults active! Set 'spring.devtools.add-properties' to 'false' to disable
2021-12-18 13:11:30.465  INFO 25823 --- [  restartedMain] .e.DevToolsPropertyDefaultsPostProcessor : For additional web related logging consider setting the 'logging.level.web' property to 'DEBUG'
2021-12-18 13:11:31.046  INFO 25823 --- [  restartedMain] .s.d.r.c.RepositoryConfigurationDelegate : Bootstrapping Spring Data JPA repositories in DEFAULT mode.
2021-12-18 13:11:31.097  INFO 25823 --- [  restartedMain] .s.d.r.c.RepositoryConfigurationDelegate : Finished Spring Data repository scanning in 45 ms. Found 2 JPA repository interfaces.
2021-12-18 13:11:31.360  INFO 25823 --- [  restartedMain] trationDelegate$BeanPostProcessorChecker : Bean 'org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler@3f7b76ab' of type [org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler] is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
2021-12-18 13:11:31.363  INFO 25823 --- [  restartedMain] trationDelegate$BeanPostProcessorChecker : Bean 'methodSecurityMetadataSource' of type [org.springframework.security.access.method.DelegatingMethodSecurityMetadataSource] is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
2021-12-18 13:11:31.602  INFO 25823 --- [  restartedMain] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
2021-12-18 13:11:31.617  INFO 25823 --- [  restartedMain] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2021-12-18 13:11:31.617  INFO 25823 --- [  restartedMain] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.52]
2021-12-18 13:11:31.682  INFO 25823 --- [  restartedMain] o.a.c.c.C.[Tomcat].[localhost].[/api]    : Initializing Spring embedded WebApplicationContext
2021-12-18 13:11:31.682  INFO 25823 --- [  restartedMain] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 1217 ms
2021-12-18 13:11:31.785  INFO 25823 --- [  restartedMain] o.f.c.internal.license.VersionPrinter    : Flyway Community Edition 7.7.3 by Redgate
2021-12-18 13:11:31.789  INFO 25823 --- [  restartedMain] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Starting...
2021-12-18 13:11:31.843  INFO 25823 --- [  restartedMain] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Start completed.
2021-12-18 13:11:31.858  INFO 25823 --- [  restartedMain] o.f.c.i.database.base.DatabaseType       : Database: jdbc:mariadb://localhost:3306/db_slaser (MariaDB 10.2)
2021-12-18 13:11:31.909  INFO 25823 --- [  restartedMain] o.f.core.internal.command.DbValidate     : Successfully validated 1 migration (execution time 00:00.019s)
2021-12-18 13:11:31.924  INFO 25823 --- [  restartedMain] o.f.core.internal.command.DbMigrate      : Current version of schema `db_slaser`: 1.0
2021-12-18 13:11:31.926  INFO 25823 --- [  restartedMain] o.f.core.internal.command.DbMigrate      : Schema `db_slaser` is up to date. No migration necessary.
2021-12-18 13:11:32.032  INFO 25823 --- [  restartedMain] o.hibernate.jpa.internal.util.LogHelper  : HHH000204: Processing PersistenceUnitInfo [name: default]
2021-12-18 13:11:32.075  INFO 25823 --- [  restartedMain] org.hibernate.Version                    : HHH000412: Hibernate ORM core version 5.5.3.Final
2021-12-18 13:11:32.188  INFO 25823 --- [  restartedMain] o.hibernate.annotations.common.Version   : HCANN000001: Hibernate Commons Annotations {5.1.2.Final}
2021-12-18 13:11:32.260  INFO 25823 --- [  restartedMain] org.hibernate.dialect.Dialect            : HHH000400: Using dialect: org.hibernate.dialect.MariaDBDialect
2021-12-18 13:11:32.698  INFO 25823 --- [  restartedMain] o.h.e.t.j.p.i.JtaPlatformInitiator       : HHH000490: Using JtaPlatform implementation: [org.hibernate.engine.transaction.jta.platform.internal.NoJtaPlatform]
2021-12-18 13:11:32.705  INFO 25823 --- [  restartedMain] j.LocalContainerEntityManagerFactoryBean : Initialized JPA EntityManagerFactory for persistence unit 'default'
2021-12-18 13:11:33.100  WARN 25823 --- [  restartedMain] JpaBaseConfiguration$JpaWebConfiguration : spring.jpa.open-in-view is enabled by default. Therefore, database queries may be performed during view rendering. Explicitly configure spring.jpa.open-in-view to disable this warning
2021-12-18 13:11:33.434  INFO 25823 --- [  restartedMain] o.s.s.web.DefaultSecurityFilterChain     : Will secure any request with [org.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFilter@e05a509, org.springframework.security.web.context.SecurityContextPersistenceFilter@1076316c, org.springframework.security.web.header.HeaderWriterFilter@60bf0d86, org.springframework.web.filter.CorsFilter@2a80e1c9, org.springframework.security.web.authentication.logout.LogoutFilter@3090b71e, com.slaser.slaserybelleza.spring.security.components.JwtRequestFilter@937c7eb, org.springframework.security.web.savedrequest.RequestCacheAwareFilter@2bf2d9bc, org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter@d829974, org.springframework.security.web.authentication.AnonymousAuthenticationFilter@7eed6a3f, org.springframework.security.web.session.SessionManagementFilter@427f654a, org.springframework.security.web.access.ExceptionTranslationFilter@3f45a45f, org.springframework.security.web.access.intercept.FilterSecurityInterceptor@74a3f174]
2021-12-18 13:11:33.653  INFO 25823 --- [  restartedMain] o.s.b.d.a.OptionalLiveReloadServer       : LiveReload server is running on port 35729
2021-12-18 13:11:33.695  INFO 25823 --- [  restartedMain] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path '/api'
2021-12-18 13:11:33.710  INFO 25823 --- [  restartedMain] c.s.s.SlaserybellezaApplication          : Started SlaserybellezaApplication in 3.579 seconds (JVM running for 4.151)
Hibernate: select count(*) as col_0_0_ from user userregist0_

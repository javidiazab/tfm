package com.slaser.slaserybelleza.Utils;

import com.slaser.slaserybelleza.utils.FileUtils;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertTrue;

class FileUtilsTest {

    @Test
    @SneakyThrows
    void shouldReadFile_whenPathProvided() throws Exception {
        //Given
        final String givenTemplate = "TestFile.html";

        //When
        String testFile = FileUtils.readString(givenTemplate);

        //Then
        assertTrue(Objects.nonNull(testFile));
    }
}
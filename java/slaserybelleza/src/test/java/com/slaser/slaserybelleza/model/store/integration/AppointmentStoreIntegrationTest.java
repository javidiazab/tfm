package com.slaser.slaserybelleza.model.store.integration;

import com.slaser.slaserybelleza.Utils.Fixtures;
import com.slaser.slaserybelleza.component.persistance.repository.AppointmentRepository;
import com.slaser.slaserybelleza.model.entity.AppointmentRegister;
import com.slaser.slaserybelleza.model.store.AppointmentStore;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@AutoConfigureMockMvc
public class AppointmentStoreIntegrationTest {

    @Autowired
    private AppointmentStore appointmentStore;

    @Autowired
    private AppointmentRepository appointmentRepository;

    @BeforeEach
    void initialize() {
        final AppointmentRegister appointment = Fixtures.validAppointmentRegister().idCli(1L).build();
        final AppointmentRegister appointment2 = Fixtures.validAppointmentRegister().idCli(1L).build();
        appointmentRepository.deleteAll();
        appointmentRepository.save(appointment);
        appointmentRepository.save(appointment2);
    }

    @Test
    @SneakyThrows
    void shouldReturnAppointment_whenSaveRequestIsProvided() throws Exception {
        //Given
        final AppointmentRegister givenAppointmentRegister = Fixtures.validAppointmentRegister().idCli(1L).build();
        //When
        final AppointmentRegister actualAppointment = appointmentStore.save(givenAppointmentRegister);

        //then
        assertEquals(3, appointmentRepository.count());
        assertEquals(givenAppointmentRegister.getIdCli(), actualAppointment.getIdCli());
        assertEquals(givenAppointmentRegister.getSubject(), actualAppointment.getSubject());
        assertEquals(givenAppointmentRegister.getPlace(), actualAppointment.getPlace());
        assertEquals(givenAppointmentRegister.getDescription(), actualAppointment.getDescription());
        assertEquals(givenAppointmentRegister.getDuration(), actualAppointment.getDuration());
        assertEquals(givenAppointmentRegister.getType(), actualAppointment.getType());
        assertEquals(givenAppointmentRegister.getStartingDate(), actualAppointment.getStartingDate());
        assertEquals(givenAppointmentRegister.getEndDate(), actualAppointment.getEndDate());
    }

    @Test
    @SneakyThrows
    void shouldReturnAppointment_whenUpdateRequestIsProvided() throws Exception {
        //Given
        final long givenId = appointmentRepository.findAll().stream().findFirst().orElseThrow().getId();
        final AppointmentRegister givenAppointmentToUpdate = Fixtures.validAppointmentRegister().id(givenId).idCli(1L).subject("new Subject").build();

        //When
        final AppointmentRegister actualAppointment = appointmentStore.update(givenAppointmentToUpdate);

        //then
        assertEquals(2, appointmentRepository.count());
        assertEquals(givenAppointmentToUpdate.getId(), actualAppointment.getId());
        assertEquals(givenAppointmentToUpdate.getIdCli(), actualAppointment.getIdCli());
        assertEquals(givenAppointmentToUpdate.getSubject(), actualAppointment.getSubject());
        assertEquals(givenAppointmentToUpdate.getPlace(), actualAppointment.getPlace());
        assertEquals(givenAppointmentToUpdate.getDescription(), actualAppointment.getDescription());
        assertEquals(givenAppointmentToUpdate.getDuration(), actualAppointment.getDuration());
        assertEquals(givenAppointmentToUpdate.getType(), actualAppointment.getType());
        assertEquals(givenAppointmentToUpdate.getStartingDate(), actualAppointment.getStartingDate());
        assertEquals(givenAppointmentToUpdate.getEndDate(), actualAppointment.getEndDate());
    }

    @Test
    @SneakyThrows
    void shouldReturnAppointment_whenIdIsProvided() throws Exception {
        //Given
        final long givenId = appointmentRepository.findAll().stream().findFirst().orElseThrow().getId();

        //When
        final AppointmentRegister actualAppointment = appointmentStore.getById(givenId).orElseThrow();

        //Then
        assertEquals(givenId, actualAppointment.getId());
    }

    @Test
    @SneakyThrows
    void shouldDeleteAppointment_whenIdIsProvided() throws Exception {
        //Given
        final long givenId = appointmentRepository.findAll().stream().findFirst().orElseThrow().getId();

        //When
        boolean actualResult = appointmentStore.delete(givenId);

        //then
        assertTrue(actualResult);
        assertEquals(1, appointmentRepository.count());
    }

    @Test
    @SneakyThrows
    void shouldReturnListOfAppointments_whenDatesAreProvided() throws Exception {
        //Given
        final LocalDateTime givenStartingDate = LocalDateTime.now().minusDays(1);
        final LocalDateTime givenEndDate = LocalDateTime.now().plusDays(1);
        //When
        final List<AppointmentRegister> actualAppointments = appointmentStore.findByDates(givenStartingDate, givenEndDate);

        //then
        assertEquals(2, actualAppointments.size());
    }

    @Test
    @SneakyThrows
    void shouldReturnListOfAppointments_whenIdAndDatesAreProvided() throws Exception {
        //Given
        final long givenId = 1L;
        final LocalDateTime givenStartingDate = LocalDateTime.now().minusDays(1);
        final LocalDateTime givenEndDate = LocalDateTime.now().plusDays(1);
        //When
        final List<AppointmentRegister> actualAppointments = appointmentStore.findByIdCliAndDates(givenId, givenStartingDate, givenEndDate);

        //then
        assertEquals(2, actualAppointments.size());
    }
}

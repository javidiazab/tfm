package com.slaser.slaserybelleza.controller.unit;

import com.slaser.slaserybelleza.controller.AppointmentController;
import com.slaser.slaserybelleza.model.client.AppointmentDto;
import com.slaser.slaserybelleza.service.appointment.AppointmentService;
import com.slaser.slaserybelleza.utils.Utils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import static com.slaser.slaserybelleza.Utils.Fixtures.validAppointment;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AppointmentControllerTest {

    @Mock
    private AppointmentService appointmentService;

    @InjectMocks
    private AppointmentController appointmentController;

    @Test
    void shouldCallSaveAppointment_whenSaveRequestIsReceived() {
        //Given
        final AppointmentDto givenAppointment = validAppointment().build();
        when(appointmentService.add(givenAppointment)).thenReturn(givenAppointment);

        //When
        final ResponseEntity<AppointmentDto> actualResponse = appointmentController.saveAppointment(givenAppointment);

        //Then
        assertEquals(HttpStatus.CREATED, actualResponse.getStatusCode());
        assertEquals(givenAppointment, actualResponse.getBody());
        verifyNoMoreInteractions(appointmentService);
    }

    @Test
    void shouldGetAppointmentDetails_whenGetAppointmentRequestIsReceived() {
        //Given
        final AppointmentDto givenAppointment = validAppointment().build();
        final long givenId = 1L;
        when(appointmentService.getById(givenId)).thenReturn(givenAppointment);

        //When
        final ResponseEntity<AppointmentDto> actualAppointmentDetails = appointmentController.getAppointmentDetails(1L);

        //Then
        assertEquals(HttpStatus.OK, actualAppointmentDetails.getStatusCode());
        assertEquals(givenAppointment, actualAppointmentDetails.getBody());
        verifyNoMoreInteractions(appointmentService);
    }

    @Test
    void shouldDeleteAppointment_whenIdIsReceived() {
        //Given
        final long givenId = 1L;
        when(appointmentService.delete(givenId)).thenReturn(true);

        //When
        final ResponseEntity<Boolean> actualResponseEntity = appointmentController.deleteAppointment(1L);

        //Then
        assertEquals(HttpStatus.ACCEPTED, actualResponseEntity.getStatusCode());
        assertEquals(true, actualResponseEntity.getBody());
        verifyNoMoreInteractions(appointmentService);
    }

    @Test
    void shouldGetAppointments_whenIdIsReceived() {
        //Given
        final List<AppointmentDto> givenAppointments = List.of(
                validAppointment().build(),
                validAppointment().build()
        );
        final String givenStart = "2021-01-01";
        final String givenEnd = "2021-01-31";
        final LocalDateTime givenStartingDate = Utils.convertToDate(givenStart);
        final LocalDateTime givenEndDate = Utils.convertToDate(givenEnd).toLocalDate().atTime(LocalTime.MAX);
        when(appointmentService.findByDates(givenStartingDate, givenEndDate))
                .thenReturn(givenAppointments);

        //When
        final ResponseEntity<List<AppointmentDto>> actualAppointments = appointmentController.getAppointments(givenStart, givenEnd);

        //Then
        assertEquals(HttpStatus.OK, actualAppointments.getStatusCode());
        assertEquals(givenAppointments, actualAppointments.getBody());
        verifyNoMoreInteractions(appointmentService);
    }

    @Test
    void shouldGetAppointmentsByCli_whenRequestIsReceived() {
        //Given
        final List<AppointmentDto> givenAppointments = List.of(
                validAppointment().build(),
                validAppointment().build()
        );
        final long givenIdCli = 1L;
        final String givenStart = "2021-01-01";
        final String givenEnd = "2021-01-31";
        final LocalDateTime givenStartingDate = Utils.convertToDate(givenStart);
        final LocalDateTime givenEndDate = Utils.convertToDate(givenEnd).toLocalDate().atTime(LocalTime.MAX);
        when(appointmentService.findByIdCliAndDates(givenIdCli, givenStartingDate, givenEndDate))
                .thenReturn(givenAppointments);

        //When
        final ResponseEntity<List<AppointmentDto>> actualAppointments = appointmentController.getAppointmentsByCli(givenIdCli, givenStart, givenEnd);

        //Then
        assertEquals(HttpStatus.OK, actualAppointments.getStatusCode());
        assertEquals(givenAppointments, actualAppointments.getBody());
        verifyNoMoreInteractions(appointmentService);
    }
}
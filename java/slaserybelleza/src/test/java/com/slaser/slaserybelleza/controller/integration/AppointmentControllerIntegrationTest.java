package com.slaser.slaserybelleza.controller.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.slaser.slaserybelleza.Utils.Fixtures;
import com.slaser.slaserybelleza.component.persistance.repository.UserRepository;
import com.slaser.slaserybelleza.model.client.AppointmentDto;
import com.slaser.slaserybelleza.model.client.UserRole;
import com.slaser.slaserybelleza.model.entity.UserRegister;
import com.slaser.slaserybelleza.service.appointment.AppointmentService;
import com.slaser.slaserybelleza.spring.security.components.JwtTokenUtil;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class AppointmentControllerIntegrationTest {

    private static final String URL_GET = "/private/appointment/{id}";
    private static final String URL_SAVE = "/private/appointment/save";
    private static final String AUTHORIZATION = "Authorization";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @BeforeEach
    void initialize() {
        final UserRegister user = Fixtures.validUserRegister().build();
        userRepository.deleteAll();
        userRepository.save(user);
    }

    @Test
    void shouldReturnAppointment_whenSaveAppointmentRequestIsProvided() throws Exception {
        final AppointmentDto givenAppointment = Fixtures.validAppointment().build();

        //When
        mockMvc.perform(MockMvcRequestBuilders
                        .post(URL_SAVE)
                        .header(AUTHORIZATION, getToken())
                        .content(objectMapper.writeValueAsString(givenAppointment))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.subject").exists());
    }

    @Test
    @SneakyThrows
    void shouldReturnAppointment_whenDetailsIdRequestIsProvided() throws Exception {
        final long givenId = 1L;
        final AppointmentDto givenAppointment = Fixtures.validAppointment().build();

        //When
        //Then
        mockMvc.perform(get(URL_GET, givenId)
                        .header(AUTHORIZATION, getToken())
                        .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
    }

    @Test
    @SneakyThrows
    void shouldThrow404WhenInvalidDetailsIdIsProvided() throws Exception {
        //Given
        final long givenId = 33333L;

        //When
        //then
        mockMvc.perform(get(URL_GET, givenId)
                        .header(AUTHORIZATION, getToken())
                        .accept(APPLICATION_JSON)).andDo(print())
                .andExpect(status().isNotFound());
    }

    private String getToken() {
        User givenUser = new User("user", "password", List.of(UserRole.USER));
        return "Bearer " + jwtTokenUtil.generateToken(givenUser);
    }
}

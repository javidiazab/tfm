package com.slaser.slaserybelleza.service.login;

import com.slaser.slaserybelleza.model.client.AuthenticationRequest;
import com.slaser.slaserybelleza.model.login.JwtResponse;
import com.slaser.slaserybelleza.service.LoginService;
import com.slaser.slaserybelleza.spring.security.components.JwtTokenUtil;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.security.auth.login.LoginException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class LoginServiceTest {

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private UserDetailsService userDetailsService;

    @Mock
    private JwtTokenUtil jwtTokenUtil;

    @InjectMocks
    private LoginService loginService;

    @Test
    @SneakyThrows
    void shoudReturnToken_whenValidUserAndPassword() throws Exception{
        //Given
        AuthenticationRequest authenticationRequest = AuthenticationRequest.builder()
                .user("user")
                .password("password")
                .build();
        when(jwtTokenUtil.generateToken(any())).thenReturn("token");

        //When
        JwtResponse actualToken = loginService.createAuthenticationToken(authenticationRequest);

        //Then
        assertEquals("token", actualToken.getToken());
        verify(authenticationManager).authenticate(any());
        verify(userDetailsService).loadUserByUsername(any());
    }

    @Test
    void shoudThrowLoginExceptionWhenUserDissabled() {
        //Given
        AuthenticationRequest authenticationRequest = AuthenticationRequest.builder()
                .user("user")
                .password("password")
                .build();
        when(authenticationManager.authenticate(any())).thenThrow(DisabledException.class);

        //When
        assertThrows(LoginException.class, () -> loginService.createAuthenticationToken(authenticationRequest));

        //Then
        verifyNoInteractions(jwtTokenUtil);
        verifyNoInteractions(userDetailsService);
    }

    @Test
    void shoudThrowLoginExceptionWhenUInvalidCredentials() {
        //Given
        AuthenticationRequest authenticationRequest = AuthenticationRequest.builder()
                .user("user")
                .password("password")
                .build();
        when(authenticationManager.authenticate(any())).thenThrow(new BadCredentialsException("Error"));

        //When
        assertThrows(LoginException.class, () -> loginService.createAuthenticationToken(authenticationRequest));

        //Then
        verifyNoInteractions(jwtTokenUtil);
        verifyNoInteractions(userDetailsService);
    }
}
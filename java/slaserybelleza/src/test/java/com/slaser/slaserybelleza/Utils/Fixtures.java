package com.slaser.slaserybelleza.Utils;

import com.slaser.slaserybelleza.model.client.AppointmentDto;
import com.slaser.slaserybelleza.model.client.AppointmentDto.AppointmentDtoBuilder;
import com.slaser.slaserybelleza.model.client.AuthenticationRequest;
import com.slaser.slaserybelleza.model.client.AuthenticationRequest.AuthenticationRequestBuilder;
import com.slaser.slaserybelleza.model.client.EmailBodyDto;
import com.slaser.slaserybelleza.model.client.EmailBodyDto.EmailBodyDtoBuilder;
import com.slaser.slaserybelleza.model.client.UserDto;
import com.slaser.slaserybelleza.model.client.UserDto.UserDtoBuilder;
import com.slaser.slaserybelleza.model.client.UserRole;
import com.slaser.slaserybelleza.model.entity.AppointmentRegister;
import com.slaser.slaserybelleza.model.entity.AppointmentRegister.AppointmentRegisterBuilder;
import com.slaser.slaserybelleza.model.entity.UserRegister;
import com.slaser.slaserybelleza.model.entity.UserRegister.UserRegisterBuilder;

import java.time.LocalDateTime;

public class Fixtures {
    public static AppointmentDtoBuilder validAppointment() {
        return AppointmentDto.builder()
                .subject("subject1")
                .place("place1")
                .description("description1")
                .duration("1 hora")
                .type("Servicio")
                .cancelDate(null)
                .startingDate(LocalDateTime.now())
                .endDate(LocalDateTime.now());
    }

    public static EmailBodyDtoBuilder validEmailBody() {
        return EmailBodyDto.builder()
                .email("test@test.com")
                .subject("Test Subject")
                .content("Content of the body");
    }

    public static AuthenticationRequestBuilder validAuthenticationRequest() {
        return AuthenticationRequest.builder()
                .user("user")
                .password("password");
    }

    public static UserDtoBuilder validUserDto() {
        return UserDto.builder()
                .userName("user")
                .password("password")
                .userFullName("full_usu_name")
                .role(UserRole.USER)
                .retries(0);
    }

    public static AppointmentRegisterBuilder validAppointmentRegister() {
        return AppointmentRegister.builder()
                .subject("subject1")
                .place("place1")
                .description("description1")
                .duration("1 hora")
                .type("Servicio")
                .cancelDate(null)
                .startingDate(LocalDateTime.now())
                .endDate(LocalDateTime.now());
    }

    public static AppointmentDtoBuilder validAppointmentDto() {
        return AppointmentDto.builder()
                .subject("subject1")
                .place("place1")
                .description("description1")
                .duration("1 hora")
                .type("Servicio")
                .cancelDate(null)
                .startingDate(LocalDateTime.now())
                .endDate(LocalDateTime.now());
    }

    public static UserRegisterBuilder validUserRegister() {
        return UserRegister.builder()
                .userName("user")
                .password("password")
                .userFullName("full_usu_name")
                .role(UserRole.USER)
                .retries(0);
    }
}

create TABLE appointment (
	id BIGINT(5) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	id_cli BIGINT,
	subject VARCHAR(200) NOT NULL,
	place VARCHAR(200) NOT NULL,
	description TEXT,
	duration VARCHAR(30) NOT NULL,
	type VARCHAR(30) NOT NULL,
    starting_date DATETIME NOT NULL,
    end_date DATETIME NOT NULL,
    cancel_date DATETIME,
    creation_date DATETIME,
    updated_date DATETIME,
    usu_mod VARCHAR(30)
);

create TABLE IF NOT EXISTS user (
    user_name VARCHAR(50) PRIMARY KEY NOT NULL,
    password VARCHAR(100) NOT NULL,
    user_full_name VARCHAR(100) NOT NULL,
    role VARCHAR(20) NOT NULL,
    retries DECIMAL,
    creation_date DATETIME,
    end_date DATETIME
    );
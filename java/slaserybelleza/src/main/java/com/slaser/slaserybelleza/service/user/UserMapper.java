package com.slaser.slaserybelleza.service.user;

import com.slaser.slaserybelleza.model.client.UserDto;
import com.slaser.slaserybelleza.model.entity.UserRegister;

import static java.util.Objects.isNull;

/**
 * Mapper from/to Dto/Register
 */
public class UserMapper {

    /**
     * <p>Generate an UserRegister to be feed to the DB</p>
     * @param userDto  User information in Dto format
     * @return User in Register format
     * @since 1.0
     */
    public static UserRegister buildUserRegister(UserDto userDto) {
        if (isNull(userDto)) {
            return null;
        }
        return UserRegister.builder()
                .userName(userDto.getUserName())
                .password(userDto.getPassword())
                .userFullName(userDto.getUserFullName())
                .role(userDto.getRole())
                .retries(userDto.getRetries())
                .creationDate(userDto.getCreationDate())
                .endDate(userDto.getEndDate())
                .build();
    }

    /**
     * <p>Generate an UserDto to be feed to the DB</p>
     * @param userRegister  User information in Register format
     * @return User in Dto format
     * @since 1.0
     */
    public static UserDto buildUserDto(UserRegister userRegister) {
        if (isNull(userRegister)) {
            return null;
        }
        return UserDto.builder()
                .userName(userRegister.getUserName())
                .password(userRegister.getPassword())
                .userFullName(userRegister.getUserFullName())
                .role(userRegister.getRole())
                .retries(userRegister.getRetries())
                .creationDate(userRegister.getCreationDate())
                .endDate(userRegister.getEndDate())
                .build();
    }
}


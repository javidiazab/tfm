package com.slaser.slaserybelleza.model.client;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * I/O for appointment endpoints
 * Appointment information
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AppointmentDto implements Serializable {

    private Long id;
    private Long idCli;
    private String subject;
    private String place;
    private String description;
    private String duration;
    private String type;
    //@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
//    @JsonSerialize(using = CustomDateSerializer.class)
    private LocalDateTime startingDate;
    //@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
//    @JsonSerialize(using = CustomDateSerializer.class)
    private LocalDateTime endDate;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
//    @JsonSerialize(using = CustomDateSerializer.class)
    private LocalDateTime cancelDate;
    @ToString.Exclude
    //@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
//    @JsonSerialize(using = CustomDateSerializer.class)
    private LocalDateTime creationDate;
    //@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
//    @JsonSerialize(using = CustomDateSerializer.class)
    private LocalDateTime updatedDate;
    private String usuMod;

//    public static void processedHubMessage(AuditDto auditDto, HubMessage<?> hubMessage) {
//        if (isNull(auditDto)) {
//            log.info("Unable to process null audit");
//            return;
//        }
//        auditDto.setHubId(Optional.ofNullable(hubMessage)
//                .map(HubMessage::paymentHubId)
//                .map(UUID::toString)
//                .orElse(null));
//        auditDto.setHubMessageType(Optional.ofNullable(hubMessage)
//                .map(Iso20022Message::definition)
//                .map(MessageDefinition::schema)
//                .orElse(null));
//        auditDto.setHubFlow(Optional.ofNullable(hubMessage)
//                .map(Iso20022Message::flow)
//                .map(Enum::name)
//                .orElse(null));
//        auditDto.setStatus(PROCESSED);
//    }
//
//    public static void processedSwiftMessage(AuditDto auditDto, AbstractMT abstractMT) {
//        if (isNull(auditDto)) {
//            log.info("Unable to process null audit");
//            return;
//        }
//
//        auditDto.setSwiftType(MappingUtils.getMessageType(abstractMT));
//        auditDto.setField20(MappingUtils.getField20(abstractMT));
//        auditDto.setField21(MappingUtils.getField21(abstractMT));
//        auditDto.setMessage(MappingUtils.getMessage(abstractMT));
//        auditDto.setStatus(PROCESSED);
//    }
//
//    public static void rejectedMessage(AuditDto auditDto, AbstractMT abstractMT) {
//        if (isNull(auditDto)) {
//            log.info("Unable to process null audit");
//            return;
//        }
//
//        auditDto.setRejectedResponseField20(MappingUtils.getField20(abstractMT));
//        auditDto.setRejectedResponseField21(MappingUtils.getField21(abstractMT));
//        auditDto.setRejectedResponseMessage(MappingUtils.getMessage(abstractMT));
//        auditDto.setStatus(REJECTED);
//    }
//
//    public static void rejectedAudit(AuditDto auditDto, AuditReason auditReason) {
//        if (isNull(auditDto)) {
//            log.info("Unable to process null audit");
//            return;
//        }
//
//        auditDto.setStatus(REJECTED);
//        auditDto.setReason(Optional.ofNullable(auditReason).orElse(null));
//    }
//
//    public static void rejectedHandlerExceptionAudit(AuditDto auditDto, Exception ex) {
//        if (isNull(auditDto)) {
//            log.info("Unable to process null audit");
//            return;
//        }
//
//        auditDto.setStatus(REJECTED);
//        auditDto.setReason(ex instanceof UnsupportedMTMessageException || ex instanceof UnsupportedISOMessageException
//                ? AuditReason.UNSUPPORTED : AuditReason.FATAL_EXCEPTION);
//        auditDto.setReasonDescription(Optional
//                .ofNullable(ex)
//                .map(Throwable::getMessage)
//                .orElse(null));
//    }
//
//    public static void duplicatedAudit(AuditDto auditDto) {
//        if (isNull(auditDto)) {
//            log.info("Unable to process null audit");
//            return;
//        }
//
//        auditDto.setStatus(AuditStatus.REJECTED);
//        auditDto.setReason(AuditReason.DUPLICATED);
//    }
//
//    public static void reportAsProcessed(AuditDto auditDto) {
//        if (isNull(auditDto)) {
//            log.info("[AuditService] Unable to process null audit");
//            return;
//        }
//
//        auditDto.setStatus(PROCESSED);
//    }
//
//    public static void setRejectedReason(AuditDto auditDto, AuditReason auditReason, String auditDescription) {
//        if (isNull(auditDto)) {
//            log.info("Unable to process null audit");
//            return;
//        }
//        auditDto.setReason(auditReason);
//        auditDto.setReasonDescription(auditDescription);
//        auditDto.setStatus(AuditStatus.REJECTED);
//    }
//
//    public static AuditDto clone(AuditDto auditDto) {
//
//        if (isNull(auditDto)) {
//            return null;
//        }
//
//        AuditDto clonedAuditDto = SerializationUtils.clone(auditDto);
//        clonedAuditDto.setId(null);
//        return clonedAuditDto;
//
//    }
}

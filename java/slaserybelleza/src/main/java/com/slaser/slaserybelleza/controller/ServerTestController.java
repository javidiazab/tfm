package com.slaser.slaserybelleza.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/**
 * Controller to test connectivity when deployed
 */
@RestController
@RequestMapping("/ping")
@Slf4j
public class ServerTestController {

    /**
     * <p>Utility to check if the access to the application is available</p>
     * Check whether the server is up and the application running
     * @return Current date and time
     * @since 1.0
     */
    @GetMapping
    public String pin(){
        log.info("Received call /ping");
        return "Conextion stablished " + LocalDateTime.now();
    }
}

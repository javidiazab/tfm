package com.slaser.slaserybelleza.model.client;

import com.slaser.slaserybelleza.utils.Constants;
import org.springframework.security.core.GrantedAuthority;

/**
 * User role information
 * Not used at this time
 */
public enum UserRole implements GrantedAuthority {
    USER(Constants.USER_ROLE),
    ADMIN(Constants.ADMIN_ROLE);

    private String value;

    UserRole(String value){
        this.value = value;
    }

    @Override
    public String getAuthority() {
        return this.value;
    }
}

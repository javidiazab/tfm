package com.slaser.slaserybelleza.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * I/O for appointment DB
 * Appointment information as DB
 */
@Entity
@Table(name = "appointment")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class AppointmentRegister {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial", length = 5)
    private Long id;
    @Column(name = "id_cli", length = 5)
    private Long idCli;
    @NonNull
    private String subject;
    @NonNull
    private String place;
    @Column(length = 1000)
    private String description;
    @NonNull
    private String duration;
    @NonNull
    private String type;
    @NonNull
    @Column(name = "starting_date")
    private LocalDateTime startingDate;
    @NonNull
    @Column(name = "end_date")
    private LocalDateTime endDate;
    @Column(name = "cancel_date")
    private LocalDateTime cancelDate;
    @CreatedDate
//    @EqualsAndHashCode.Exclude
    @Column(name = "creation_date")
    private LocalDateTime creationDate;
    @LastModifiedDate
    @Column(name = "updated_date")
    private LocalDateTime updatedDate;
    @LastModifiedBy
    @Column(name = "usu_mod")
    private String usuMod;
}

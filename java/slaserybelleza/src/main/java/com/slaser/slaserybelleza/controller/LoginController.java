package com.slaser.slaserybelleza.controller;

import com.slaser.slaserybelleza.model.client.AuthenticationRequest;
import com.slaser.slaserybelleza.model.login.JwtResponse;
import com.slaser.slaserybelleza.service.LoginService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller to manage login acess
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/login")
@Slf4j
public class LoginController {

    private final LoginService loginService;

    /**
     * <p>Login access for slaserybelleza app</p>
     * @param authenticationRequest User and password to login
     * @return user name and token to be used in the following requests
     * @throws Exception If any exception occurs
     * @since 1.0
     */
    @PostMapping
    public ResponseEntity<JwtResponse> login(@NonNull @RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        log.info("Received call /login: " + authenticationRequest);
        JwtResponse token = loginService.createAuthenticationToken(authenticationRequest);
        return new ResponseEntity<>(token, HttpStatus.ACCEPTED);
    }
}

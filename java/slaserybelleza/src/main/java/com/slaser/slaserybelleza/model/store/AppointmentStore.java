package com.slaser.slaserybelleza.model.store;

import com.slaser.slaserybelleza.component.persistance.repository.AppointmentRepository;
import com.slaser.slaserybelleza.exception.SLaserException;
import com.slaser.slaserybelleza.model.entity.AppointmentRegister;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class AppointmentStore {

    private final AppointmentRepository appointmentRepository;

    public AppointmentRegister save(AppointmentRegister appointmentRegister) throws SLaserException {
        if (nonNull(appointmentRegister.getId())) {
            throw new SLaserException("Appointment id is not null");
        }
        return appointmentRepository.save(appointmentRegister);
    }

    public AppointmentRegister update(AppointmentRegister appointmentRegister) throws SLaserException {
        if (isNull(appointmentRegister.getId())) {
            throw new SLaserException("Appointment id is null");
        }
        final Optional<AppointmentRegister> appointmentRegisterById =
                appointmentRepository.findById(appointmentRegister.getId());
        if (appointmentRegisterById.isEmpty()) {
            throw new SLaserException("Appointment id not found");
        }
        return appointmentRepository.save(appointmentRegister);
    }

    public Optional<AppointmentRegister> getById(long id) throws SLaserException {
        if (isNull(id)) {
            throw new SLaserException("Appointment id is null");
        }
        return appointmentRepository.findById(id);
    }

    public boolean delete(long id) throws SLaserException {
        if (!appointmentRepository.existsById(id)) {
            return false;
        }
        appointmentRepository.deleteById(id);
        return true;
    }

    public List<AppointmentRegister> findByDates(LocalDateTime startingDate, LocalDateTime endDate) {
        final List<AppointmentRegister> list = appointmentRepository.findAllByStartingDateGreaterThanEqualAndEndDateLessThanEqualOrderByStartingDate(
                startingDate, endDate);
        return list;
    }

    public List<AppointmentRegister> findByIdCliAndDates(
            Long idCli, LocalDateTime startingDate, LocalDateTime endDate) throws SLaserException {
        if (isNull(idCli)) {
            throw new SLaserException("Appointment id_cli is null");
        }
        return appointmentRepository
                .findAllByIdCliAndStartingDateGreaterThanEqualAndEndDateLessThanEqualOrderByStartingDateAsc(
                        idCli, startingDate, endDate);
    }

    private AppointmentRegister generateAppointmentRegister(Long id, String subject, String place, String description,
                                                            String duration, String type, LocalDateTime startingDate,
                                                            LocalDateTime endDate) {
        return AppointmentRegister.builder()
                .id(id)
                .subject(subject)
                .place(place)
                .description(description)
                .duration(duration)
                .type(type)
                .startingDate(startingDate)
                .endDate(endDate)
                .build();
    }

}

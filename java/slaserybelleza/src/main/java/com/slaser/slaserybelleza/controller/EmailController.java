package com.slaser.slaserybelleza.controller;

import com.slaser.slaserybelleza.model.client.EmailBodyDto;
import com.slaser.slaserybelleza.service.email.EmailSenderService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller to manage emails
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/public/email")
@Slf4j
public class EmailController {

    private final EmailSenderService emailSenderService;
    //private final EmailService emailContactService;

    /**
     * <p>Appointment to be saved on DB</p>
     * @param emailBodyDto  Email information: email, subject and content of the email
     * @return HttpStatus
     * @since 1.0
     */
    @PostMapping("/send")
    public ResponseEntity SendEmail(@NonNull @RequestBody EmailBodyDto emailBodyDto) {
        log.info("Received call /send mail: " + emailBodyDto);
        boolean resultMail = emailSenderService.sendEmail(emailBodyDto);
        //boolean resultMail = emailContactService.sendEmail(emailBodyDto);
        return new ResponseEntity(resultMail, HttpStatus.ACCEPTED);
    }
}

package com.slaser.slaserybelleza.exception;

/**
 * Application exceptions
 */
public class SLaserException extends RuntimeException {

    /**
     * <p>General application exception</p>
     * @param errorMessage  An error message
     * @since 1.0
     */
    public SLaserException(String errorMessage) {
        super(errorMessage);
    }

    public SLaserException(String errorMessage, Throwable throwable) {
        super(errorMessage, throwable);
    }
}

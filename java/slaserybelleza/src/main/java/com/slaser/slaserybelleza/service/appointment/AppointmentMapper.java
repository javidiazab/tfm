package com.slaser.slaserybelleza.service.appointment;

import com.slaser.slaserybelleza.model.client.AppointmentDto;
import com.slaser.slaserybelleza.model.entity.AppointmentRegister;

import static java.util.Objects.isNull;

/**
 * Mapper from/to Dto/Register
 */
public class AppointmentMapper {

    /**
     * <p>Generate an AppointmentRegister to be feed to the DB</p>
     * @param appointmentDto  Appointment information in Dto format
     * @return Appointment in Register format
     * @since 1.0
     */
    public static AppointmentRegister buildAppointmentRegister(AppointmentDto appointmentDto) {

        if (isNull(appointmentDto)) {
            return null;
        }
        return AppointmentRegister.builder()
                .id(appointmentDto.getId())
                .idCli((appointmentDto.getIdCli()))
                .subject(appointmentDto.getSubject())
                .place(appointmentDto.getPlace())
                .description(appointmentDto.getDescription())
                .duration(appointmentDto.getDuration())
                .type(appointmentDto.getType())
                .startingDate(appointmentDto.getStartingDate())
                .endDate(appointmentDto.getEndDate())
                .cancelDate(appointmentDto.getCancelDate())
                .usuMod(appointmentDto.getUsuMod())
                .build();
    }

    /**
     * <p>Generate an AppointmentDto to be returned to frontend of the application</p>
     * @param appointmentRegister  Appointment information in Register format
     * @return Appointment in Dto format
     * @since 1.0
     */
    public static AppointmentDto buildAppointmentDto(AppointmentRegister appointmentRegister) {
        if (isNull(appointmentRegister)) {
            return null;
        }
        return AppointmentDto.builder()
                .id(appointmentRegister.getId())
                .idCli((appointmentRegister.getIdCli()))
                .subject(appointmentRegister.getSubject())
                .place(appointmentRegister.getPlace())
                .description(appointmentRegister.getDescription())
                .duration(appointmentRegister.getDuration())
                .type(appointmentRegister.getType())
                .startingDate(appointmentRegister.getStartingDate())
                .endDate(appointmentRegister.getEndDate())
                .cancelDate(appointmentRegister.getCancelDate())
                .creationDate(appointmentRegister.getCreationDate())
                .updatedDate(appointmentRegister.getUpdatedDate())
                .usuMod(appointmentRegister.getUsuMod())
                .build();
    }
}


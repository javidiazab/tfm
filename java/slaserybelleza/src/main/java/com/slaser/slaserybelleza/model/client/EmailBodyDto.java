package com.slaser.slaserybelleza.model.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * I/O for email endpoints
 * Email information
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmailBodyDto {
    private String email;
    private String subject;
    private String content;
}

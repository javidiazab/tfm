package com.slaser.slaserybelleza.model.entity;

import com.slaser.slaserybelleza.model.client.UserDto;
import com.slaser.slaserybelleza.model.client.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * I/O for users DB
 * User information as DB
 */
@Entity
@Table(name = "user")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class UserRegister {

    @Id
    @NonNull
    @Column(name = "user_name")
    private String userName;
    @NonNull
    private String password;
    @NonNull
    @Column(name = "user_full_name")
    private String userFullName;
    @NonNull
    @Enumerated(value= EnumType.STRING)
    private UserRole role;
    @NonNull
    private int retries;
    @CreatedDate
//    @EqualsAndHashCode.Exclude
    @Column(name = "creation_date")
    private LocalDateTime creationDate;
    @Column(name = "end_date")
    private LocalDateTime endDate;

    public UserDto toUserDto() {
        return UserDto.builder()
                .userName(this.getUserName())
                .password(this.getPassword())
                .userFullName(this.getUserFullName())
                .role(this.getRole())
                .retries(this.getRetries())
                .creationDate(this.getCreationDate())
                .endDate(this.getEndDate())
                .build();
    }
}

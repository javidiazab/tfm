package com.slaser.slaserybelleza.model;

import com.slaser.slaserybelleza.model.client.AppointmentDto;
import lombok.Builder;
import lombok.Data;

/**
 * Output for SLaser
 * Not used at this moment
 */
@Data
@Builder
public class ResponseSLaser {
    AppointmentDto appointmentDto;
}

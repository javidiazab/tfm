package com.slaser.slaserybelleza.service.email;

import com.slaser.slaserybelleza.model.client.EmailBodyDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * MIME emails
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class EmailSenderService implements EmailPort {

    private final JavaMailSender sender;

    @Value("${application.send-emails-from}")
    private String sendEmailFrom;

    /**
     * <p>Send an email with the information specified</p>
     * @param emailBody  Email info: email to, subject and content
     * @return boolean value indicating whether the email was sent or not
     * @since 1.0
     */
    @Override
    public boolean sendEmail(EmailBodyDto emailBody)  {
        log.info("EmailBody: {}", emailBody.toString());
        return sendEmailTool(emailBody.getContent(),emailBody.getEmail(), emailBody.getSubject());
    }

    private boolean sendEmailTool(String textMessage, String email,String subject) {
        boolean send = false;
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        try {
            helper.setTo(email);
            helper.setFrom(sendEmailFrom);
            helper.setText(textMessage, true);
            helper.setSubject(subject);
            sender.send(message);
            send = true;
        } catch (MessagingException exception) {
            log.error("There was an error sending MIME mail", exception);
        }
        log.info("Mail sent!");
        return send;
    }
}

package com.slaser.slaserybelleza.service.email;

import com.slaser.slaserybelleza.exception.SLaserException;
import com.slaser.slaserybelleza.model.client.EmailBodyDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

/**
 * Simple Emails
 * Not used at this time
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class EmailSimplerService implements EmailPort {

    private final JavaMailSender emailSender;

    @Value("${application.send-emails-from}")
    private String sendEmailsFrom;

    /**
     * <p>Send an email with the information specified</p>
     * @param emailBodyDto  Email info: email to, subject and content
     * @return boolean value indicating whether the email was sent or not
     * @since 1.0
     */
    @Override
    public boolean sendEmail(EmailBodyDto emailBodyDto) {
        boolean send = false;
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom(sendEmailsFrom);
            message.setTo(emailBodyDto.getEmail());
            message.setText(emailBodyDto.getContent());
            message.setSubject(emailBodyDto.getSubject());
            emailSender.send(message);
            send = true;
        } catch (SLaserException exception) {
            log.error("There was an error sending SIMPLE mail", exception);
        }
        return send;
    }
}
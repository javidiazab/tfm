package com.slaser.slaserybelleza.service.user;

import com.slaser.slaserybelleza.model.client.UserDto;
import com.slaser.slaserybelleza.model.store.UserStore;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Temporary Service to handle users
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {

    private final UserStore userStore;

    /**
     * <p>User to be saved on DB</p>
     * @param userDto  User data
     * @return The same user with an id
     * @since 1.0
     */
    public UserDto save(UserDto userDto) {
        return userStore.save(userDto);
    }

    /**
     * <p>User to be retrieved from DB</p>
     * @param userName  UserName of the user to be retrieved'
     * @return Requested user information
     * @since 1.0
     */
    public UserDto getById(String userName) {
        return userStore.findById(userName);
    }

    /**
     * <p>User to be deleted from DB</p>
     * @param userName  UserName of the user to be deleted
     * @return boolean value indicating whether de deletion was successful
     * @since 1.0
     */
    public boolean delete(String userName) {
        return userStore.delete(userName);
    }
}

# S-Laser y Belleza

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.2.

## Mustach
para instalar hay que instalar
npm install mustache --save
y
npm install --save @types/mustache
mustache es la biblioteca que contiene lo que necesitas, pero esta hecha en js puro, por lo que no tienes acceso a la sintaxis desde typescript
el de types contiene las definiciones para saber las clases y los tipos de datos, que necesitas para usarlo en typescript

#Colors
Pinky:
HEX : #e0aa92
RGB : 224, 170, 146
Black:
HEX : #363b39
RGB : 54, 59, 57

## Include Bootstrap
https://fbellod.medium.com/como-integrar-el-framework-bootstrap-en-un-proyecto-angular-a5d53fa79e03

Ejecutar: `npm install bootstrap jquery @popperjs/core`

Incluir en angular.json:

`"styles": [
"node_modules/bootstrap/dist/css/bootstrap.min.css",
"src/styles.scss"
],`
`

`"scripts": [
"node_modules/jquery/dist/jquery.min.js",
"node_modules/@popperjs/core/dist/umd/popper.min.js",
"node_modules/bootstrap/dist/js/bootstrap.min.js"
]`

## CRUD example with Angular
https://www.bezkoder.com/angular-10-crud-app/

## DOM example with Angular
http://blog.enriqueoriol.com/2017/08/angular-dom-renderer.html

## styling with angular
https://medium.com/swlh/6-ways-to-dynamically-style-angular-components-b43e037852fa

## Angular Material
https://material.angular.io/

## Bootstrap icons
First install the bootstrap icons package (--save adds it to your dependencies as well):

$ npm i bootstrap-icons --save
Then add this line to your styles.css file:
@import "~bootstrap-icons/font/bootstrap-icons.css";

From now on you can use it anywhere in your app, just like intended by the bootstrap documentation:
`<i class="bi bi-star-fill"></i>`


## Pending
Eliminar referencias a date-fns y date-format
Que es esto?
function paintShape({ shape, xPos = 0, yPos = 0 }: PaintOptions) {


##Analize Angular bundlesçIt’s super easy to get this graph.
npm install -g webpack-bundle-analyzer
In your Angular app, run ng build --stats-json (don’t use flag --prod). By enabling --stats-json you will get an additional file stats.json
Finally, run webpack-bundle-analyzer ./dist/stats.json and your browser will pop up the page at localhost:8888. Have fun with it.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

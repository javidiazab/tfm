// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  slaserEmail: 'info@slaserybelleza.es',
  slaserEmailTecnico0: 'sonia@slaserybelleza.es',
  slaserEmailTecnico1: 'sara@slaserybelleza.es',
  slaserEmailTecnico2: 'noelia@slaserybelleza.es',
  slaserEmailSubject: 'Solicitud de información desde la web',
  telephone: '+34 626 449 352',
  telephoneTecnico1: '+34 626 449 352',
  telephoneTecnico2: '+34 626 449 352',
  address: {address1: 'C. Virgen de las viñas, 17', address2: 'Madrid 28031'},
  instagram: 'https://www.instagram.com/s.laserybelleza/',

  contextPath: '/api',
  sLaserUrl: 'http://localhost:8080',  // URL to web api

  appointmentUrl: '/private/appointment',
  listUrl: '/list',
  saveUrl: '/save',
  deleteUrl: '/delete/',

  userUrl: 'private/user',

  loginUrl: '/login',

  contactUrl: '/public/email/send',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.

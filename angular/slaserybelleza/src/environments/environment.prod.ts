export const environment = {
  production: true,

  slaserEmail: 'info@slaserybelleza.es',
  slaserEmailTecnico0: 'sonia@slaserybelleza.es',
  slaserEmailTecnico1: 'sara@slaserybelleza.es',
  slaserEmailTecnico2: 'noelia@slaserybelleza.es',
  slaserEmailSubject: 'Solicitud de información desde la web',
  telephone: '+34 626 449 352',
  telephoneTecnico1: '+34 626 449 352',
  telephoneTecnico2: '+34 626 449 352',
  address: {address1: 'C. Virgen de las viñas, 17', address2: 'Madrid 28031'},
  instagram: 'https://www.instagram.com/s.laserybelleza/',

  contextPath: '',
  sLaserUrl: 'https://www.slaserybelleza.es/api',  // URL to web api

  appointmentUrl: '/private/appointment',
  listUrl: '/list',
  saveUrl: '/save',
  deleteUrl: '/delete/',

  userUrl: 'private/user',

  loginUrl: '/login',

  contactUrl: '/public/email/send',
};

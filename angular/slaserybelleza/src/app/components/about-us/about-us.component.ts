import {Component, OnInit} from '@angular/core';
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {
  public contactUrl: string;
  public telephone: string;
  public telephoneTecnico1: string;
  public telephoneTecnico2: string;
  public contactEmailTecnico0: string;
  public contactEmailTecnico1: string;
  public contactEmailTecnico2: string;

  constructor() {
    this.contactUrl = environment.slaserEmail;
    this.telephone = environment.telephone;
    this.telephoneTecnico1 = environment.telephoneTecnico1;
    this.telephoneTecnico2 = environment.telephoneTecnico2;
    this.contactEmailTecnico0 = environment.slaserEmailTecnico0;
    this.contactEmailTecnico1 = environment.slaserEmailTecnico1;
    this.contactEmailTecnico2 = environment.slaserEmailTecnico2;
  }

  ngOnInit(): void {
  }

  public contact(emailto: string) {
    var mail = document.createElement("a");
    mail.href = "mailto:" + emailto + "?subject=" + environment.slaserEmailSubject + "&body=Hola, \n";
    mail.click();
  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-appointment',
  templateUrl: './add-appointment.component.html',
  styleUrls: ['./add-appointment.component.css']
})
export class AddAppointmentComponent implements OnInit {
  comienzaDate: string;
  comienzaTime: string;

  constructor() {
    let date: Date = new Date();
    this.comienzaDate = date.toISOString().split('T')[0];
    this.comienzaTime = date.getHours() + ":" + date.getMinutes();
  }

  ngOnInit(): void {
  }
}

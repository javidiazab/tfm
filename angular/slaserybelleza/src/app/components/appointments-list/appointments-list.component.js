"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppointmentsListComponent = void 0;
var core_1 = require("@angular/core");
var appointment_1 = require("../../model/appointment");
var Utils_1 = require("../../utils/Utils");
var calendar_date_1 = require("../../model/calendar.date");
var AppointmentsListComponent = /** @class */ (function () {
    function AppointmentsListComponent(appointmentService, activatedRoute, router) {
        var _this = this;
        this.appointmentService = appointmentService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        console.log("appointments-list constructor  ------------------");
        this.title = "Citas";
        this.date1 = new Date();
        this.date2 = new Date();
        this.listDay = "";
        this.idToDelete = 0;
        this.calendarDate = new calendar_date_1.CalendarDate(0, 0, 0, false, false, new Date(), []);
        // collecting several parameters:
        //   this.router.queryParams.subscribe((params: Params) => {
        //   this.date1 = new Date(params['date1']);
        //   this.date2 = new Date(params['date2']);
        // }
        this.activatedRoute.queryParams
            .subscribe(function (params) {
            _this.calendarDate = JSON.parse(params['calendarDate']);
            _this.calendarDate.date = new Date(_this.calendarDate.date);
            _this.calendarDate.appointments.forEach(function (x) {
                x.startingDate = new Date(x.startingDate);
                x.endDate = new Date(x.endDate);
                x.cancelDate = x.cancelDate ? new Date(x.cancelDate) : undefined;
            });
            _this.listDay = Utils_1.dateFormatDSLong(_this.calendarDate.date);
            console.log("params " + _this.calendarDate.date);
            console.log("params " + _this.calendarDate.col);
            console.log("params " + _this.calendarDate.row);
            console.log("params " + _this.calendarDate.day);
            console.log("params " + _this.calendarDate.today);
            console.log("params " + _this.calendarDate.enabled);
            console.log("params " + _this.calendarDate.appointments.length);
            if (_this.calendarDate.appointments.length > 0) {
                _this.calendarDate.appointments.forEach(function (x) { return console.log("dd" + x.subject + x.startingDate); });
            }
            //this.calendarDate = params['calendarDate'];
            console.log("calendarDate " + _this.calendarDate.col);
            _this.ngOnInit();
        }, function (error) { return console.log("Error loading - appointments-list"); }, function () { return console.log("Complete notification - appointments-list"); });
    }
    AppointmentsListComponent.prototype.ngOnInit = function () {
        // this.getAppointmentsList(this.calendarDate!.date, this.calendarDate!.date);
        //this.router.params.getmap(res => res.json());
    };
    AppointmentsListComponent.prototype.addAppointment = function () {
        var _a, _b, _c;
        (_a = this.calendarDate.appointments) === null || _a === void 0 ? void 0 : _a.push(new appointment_1.Appointment("", "", "", "1 hora", "Servicio", new Date(), new Date()));
        this.ngOnInit(); //TODO Reload list
        console.log("en addAppointment " + ((_b = this.calendarDate.appointments) === null || _b === void 0 ? void 0 : _b.length));
        (_c = this.calendarDate.appointments) === null || _c === void 0 ? void 0 : _c.forEach(function (x) { return console.log("add 1 ", x.subject); });
        //this.router.navigate(['/','appointments']);
    };
    AppointmentsListComponent.prototype.deleteAppointment = function (id) {
        //TODO add confirmation
        console.log("en deleteAppointment con id: " + id);
        console.log("en deleteAppointment con id: " + this.idToDelete);
        this.appointmentService
            .deleteAppointment(this.idToDelete)
            .subscribe(function (response) { return response.id; }, function (error) { return console.log("Error delete appointment " + id); }, function () { return console.log("complete delete appointment " + id); });
        return undefined;
    };
    __decorate([
        core_1.Input()
    ], AppointmentsListComponent.prototype, "idToDelete");
    AppointmentsListComponent = __decorate([
        core_1.Component({
            selector: 'app-appointments-list',
            templateUrl: './appointments-list.component.html',
            styleUrls: ['./appointments-list.component.css']
        })
    ], AppointmentsListComponent);
    return AppointmentsListComponent;
}());
exports.AppointmentsListComponent = AppointmentsListComponent;

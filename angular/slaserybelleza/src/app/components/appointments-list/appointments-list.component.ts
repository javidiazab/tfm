import {Component, Input, OnInit} from '@angular/core';
import {Appointment} from '../../model/appointment';
import {AppointmentService} from "../../services/appointment/appointment.service";
import {ActivatedRoute, Params} from '@angular/router';
import {dateFormatDSLong} from "../../utils/Utils";
import {CalendarDate} from "../../model/calendar.date";


@Component({
  selector: 'app-appointments-list',
  templateUrl: './appointments-list.component.html',
  styleUrls: ['./appointments-list.component.css']
})

export class AppointmentsListComponent implements OnInit {
  @Input() idToDelete: number;
  public title: string;
  public listDay: string;
  public saved: boolean;
  public cancelled: Appointment | undefined;
  public calendarDate: CalendarDate;
  public appointmentToDelete: Appointment;

  constructor(private appointmentService: AppointmentService, private activatedRoute: ActivatedRoute) {
    this.title = "Citas";
    this.listDay = "";
    this.saved = false;
    this.cancelled = undefined;
    this.idToDelete = 0;
    this.calendarDate = new CalendarDate(0, 0, 0, false, false, new Date(), []);
    this.appointmentToDelete = new Appointment("", "", "", "", "", new Date(), new Date());
// collecting several parameters:
//   this.router.queryParams.subscribe((params: Params) => {
//   this.date1 = new Date(params['date1']);
//   this.date2 = new Date(params['date2']);
// }
    this.activatedRoute.queryParams
      .subscribe((params) => {
          this.calendarDate = JSON.parse(params['calendarDate']);
          this.calendarDate.date = new Date(this.calendarDate.date);
          this.calendarDate.appointments.forEach(x => {
            console.log("   Lista: ", this.calendarDate.appointments);
            x.startingDate = new Date(x.startingDate);
            x.endDate = new Date(x.endDate);
            x.cancelDate = x.cancelDate ? new Date(x.cancelDate) : undefined;
          });

          this.listDay = dateFormatDSLong(this.calendarDate.date);

          // if (this.calendarDate.appointments.length > 0) {
          //   this.calendarDate.appointments.forEach(x => console.log("dd" + x.subject + x.startingDate));
          // }
          //this.calendarDate = params['calendarDate'];
          //console.log("calendarDate " + this.calendarDate.col);
          // this.ngOnInit();
        },
        error => console.log("Error loading - appointments-list"),
        () => console.log("Complete notification - appointments-list")
      );
  }

  ngOnInit() {
    // this.getAppointmentsList(this.calendarDate!.date, this.calendarDate!.date);
    //this.router.params.getmap(res => res.json());
  }

  updateAppointment(updatedAppointment: Appointment) {
    let index = this.calendarDate.appointments.findIndex(apmt =>apmt.id === updatedAppointment.id);
    if (index >= 0) {
      this.calendarDate.appointments[index].subject = updatedAppointment.subject;
      this.calendarDate.appointments[index].place = updatedAppointment.place;
      this.calendarDate.appointments[index].description = updatedAppointment.description;
      this.calendarDate.appointments[index].duration = updatedAppointment.duration;
      this.calendarDate.appointments[index].type = updatedAppointment.type;
      this.calendarDate.appointments[index].cancelDate = updatedAppointment.cancelDate;
      this.calendarDate.appointments[index].startingDate = updatedAppointment.startingDate;
      this.calendarDate.appointments[index].endDate = updatedAppointment.endDate;
    }
  }

  addAppointment(date: Date) {
    this.calendarDate.appointments?.push(new Appointment("", "", "", "1 hora", "Servicio", date, date));
    this.ngOnInit();
    this.calendarDate.appointments?.forEach(x => console.log("add 1 ", x.subject));
  }

  confirmDelete(appointment: Appointment) {
    this.appointmentToDelete = appointment;
  }

  deleteAppointment(idToDetele: number | undefined): number | undefined { //TODO use idToDelete
    let id = this.appointmentToDelete.id;
    if (id) {
      this.appointmentService
        .deleteAppointment(id)
        .subscribe(response => {
            let toDelete = this.calendarDate.appointments.findIndex(d => d.id == id);
            this.calendarDate.appointments.splice(toDelete, 1);
            this.ngOnInit();
          },
          error => console.log("Error delete appointment " + id),
          () => console.log("complete delete appointment " + id)
        );
      return undefined;
    }
    return undefined;
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyshomeComponent } from './companyshome.component';

describe('CompanyshomeComponent', () => {
  let component: CompanyshomeComponent;
  let fixture: ComponentFixture<CompanyshomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyshomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyshomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

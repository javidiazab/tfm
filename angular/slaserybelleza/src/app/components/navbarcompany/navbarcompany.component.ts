import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from "../../services/login/login.service";

@Component({
  selector: 'app-navbarcompany',
  templateUrl: './navbarcompany.component.html',
  styleUrls: ['./navbarcompany.component.css']
})
export class NavbarcompanyComponent implements OnInit {
  public userFullName: string | null;

  constructor(private router: Router, private loginService: LoginService) {
    this.userFullName = localStorage.getItem('userName');
  }

  ngOnInit(): void {

  }

  public logOut() {
    this.loginService.logOut();
    this.router.navigate(['/home']);
  }
}

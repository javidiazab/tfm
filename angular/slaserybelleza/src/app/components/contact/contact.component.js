"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ContactComponent = void 0;
var core_1 = require("@angular/core");
var EmailContactContentDto_1 = require("../../model/EmailContactContentDto");
var ContactComponent = /** @class */ (function () {
    function ContactComponent(location, emailService) {
        this.location = location;
        this.emailService = emailService;
        this.emailContactContent = new EmailContactContentDto_1.EmailContactContentDto();
    }
    ;
    ContactComponent.prototype.ngOnInit = function () {
    };
    ContactComponent.prototype.back = function () {
        this.location.back();
    };
    ContactComponent.prototype.sendMail = function () {
        var _this = this;
        this.validateContactForm(this.emailContactContent);
        this.emailService.sendContactEmail(this.emailContactContent)
            .subscribe(function (response) { return console.log("Respuesta" + response); }, function (error) { return console.log("Error sending contact email" + _this.emailContactContent); }, function () { return ("Complete sending contact email" + _this.emailContactContent); });
        this.back();
    };
    ContactComponent.prototype.validateContactForm = function (emailForm) {
        console.log("validando " + emailForm.email);
        var errorMsg = "";
        if (!emailForm.name) {
            errorMsg = "Error, el campo nombre debe ser informado";
        }
        if (!emailForm.telephone && !emailForm.email) {
            errorMsg += "\nError, teléfono o email deben ser informados";
        }
        if (!emailForm.comment) {
            errorMsg += "\nError, debe introducir un comentario";
        }
        return errorMsg;
    };
    ContactComponent = __decorate([
        core_1.Component({
            selector: 'app-contact',
            templateUrl: './contact.component.html',
            styleUrls: ['./contact.component.css']
        })
    ], ContactComponent);
    return ContactComponent;
}());
exports.ContactComponent = ContactComponent;

import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common'
import {EmailContactContentDto} from "../../model/EmailContactContentDto";
import {EmailService} from "../../services/contact/email.service";
import {Modal} from "bootstrap";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  public contactUrl: string;
  public telephone: string;
  public emailContactContent: EmailContactContentDto;
  public modal: Modal | null = null;

  constructor(private location: Location, private emailService: EmailService) {
    this.contactUrl = environment.slaserEmail;
    this.telephone = environment.telephone;
    this.emailContactContent = new EmailContactContentDto();
  };

  ngOnInit(): void {
    const obj = document.getElementById('contactModal');

    if (!obj) {
      console.log('Modal not found')
      return;
    }

    this.modal = new Modal(obj);
  }

  openModal() {
    if (this.modal) {
      this.modal.show();
    }
  }

  close() {
    if (this.modal) {
      this.modal.hide();
    }
  }

  onSubmit(): void {
    if (this.validateContactForm(this.emailContactContent) != "") {
      console.log("Error validating");
    } else {
      this.emailService.sendContactEmail(this.emailContactContent)
        .subscribe(response => console.log("Response" + response),
          (error) => console.log("Error sending contact email" + this.emailContactContent),
          () => ("Complete sending contact email" + this.emailContactContent)
        );
      this.close();
    }
  }

  validateContactForm(emailForm: EmailContactContentDto): string {
    let errorMsg: string = '';
    if (!emailForm.telephone && !emailForm.email) {
      errorMsg = "Error, field name must be informed";
    }
    return errorMsg;
  }

  public contact(emailto: string) {
    var mail = document.createElement("a");
    mail.href = "mailto:" + emailto + "?subject=" + environment.slaserEmailSubject + "&body=Hola, \n";
    mail.click();
  }
}

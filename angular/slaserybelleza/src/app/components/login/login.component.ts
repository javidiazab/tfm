import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Location} from '@angular/common'
import {Router} from '@angular/router';
import {Modal} from 'bootstrap';
import {LoginService} from "../../services/login/login.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoginResponse} from "../../model/loginResponse";
import {HttpErrorResponse} from "@angular/common/http";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @Output() onSave: EventEmitter<void> = new EventEmitter();
  modal: Modal | null = null;
  public loginModal!: Modal;
  private inProgress: boolean = false;
  public form: FormGroup;
  private navigateTo: string;

  constructor(private location: Location, private router: Router, private loginService: LoginService,
              private fb: FormBuilder) {
    this.navigateTo = '';
    this.form = this.fb.group({
      uname: ['', [Validators.required, Validators.minLength(3)]],
      psw: ['', [Validators.required, Validators.minLength(4)]]
    });
  }

  ngOnInit(): void {
    const objLoginModal = document.getElementById('loginModal');
    if (!objLoginModal) {
      console.log('Modal not found')
      return;
    }
    this.modal = new Modal(objLoginModal);
  }

  get uname() {
    return this.form.get('uname');
  }

  get psw() {
    return this.form.get('psw');
  }

  openModal(navigateTo: string) {
    this.navigateTo = navigateTo;
    if (localStorage.getItem('token')) {
      this.router.navigate(['/', this.navigateTo]);
    } else {
      if (this.modal) {
        this.modal.show();
      }
    }
  }

  onSubmit() {
    this.inProgress = true;
    //const formLogin = this.form.value;
    this.login(this.uname?.value, this.psw?.value)
  }

  close() {
    if (this.modal) {
      this.modal.hide();
    }
  }

  login(uname: string, psw: string) {
    this.loginService
      .getAccessFromServer(uname, psw)
      .subscribe((response: LoginResponse | any) => {
          if (response.token) {
              console.log("En login ok ", localStorage.getItem('token'));
            this.router.navigate(['/', this.navigateTo]);
            this.close();
          } else {
            if(response instanceof HttpErrorResponse) {
              console.log("En login ko")
              if (response.status == 500) {
                this.form.controls['uname'].setErrors({'autenticationError': true});
              }
              if (response.status == 401) {
                this.form.controls['psw'].setErrors({'passwordError': true});
              }
              if (response.status != 401 && response.status != 403) {alert("Error: " + response.status)}
            }
            this.inProgress = false;
          }
        },
        (error) => {console.log("Error login", error);},
        () => console.log("Completed login")
      );
  }
}

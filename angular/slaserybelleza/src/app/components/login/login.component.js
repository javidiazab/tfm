"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.LoginComponent = void 0;
var core_1 = require("@angular/core");
var bootstrap_1 = require("bootstrap");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(location, router) {
        this.location = location;
        this.router = router;
        this.onSave = new core_1.EventEmitter();
        this.modal = null;
        this.usu = "";
        this.password = "";
        this.displayStyle = 'block';
    }
    LoginComponent.prototype.ngOnInit = function () {
        var obj = document.getElementById('loginId');
        if (!obj) {
            console.log('Modal not found');
            return;
        }
        this.modal = new bootstrap_1.Modal(obj);
    };
    LoginComponent.prototype.openModal = function () {
        if (this.modal) {
            this.modal.show();
        }
    };
    LoginComponent.prototype.onSubmit = function () {
        console.log("logueado ..... " + this.usu + " " + this.password);
        this.close();
        this.router.navigate(['/', 'companyshome']);
    };
    LoginComponent.prototype.close = function () {
        if (this.modal) {
            this.modal.hide();
        }
        //this.onSave.emit();
    };
    __decorate([
        core_1.Output()
    ], LoginComponent.prototype, "onSave");
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.css']
        })
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;

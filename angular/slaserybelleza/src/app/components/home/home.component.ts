import {Component, OnInit, ViewChild} from '@angular/core';
import {LoginComponent} from "../login/login.component";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild(LoginComponent) private loginComponent!: LoginComponent;

  constructor() {

  }

  ngOnInit(): void {
  }
}

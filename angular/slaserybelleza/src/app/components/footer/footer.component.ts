import { Component, OnInit } from '@angular/core';
import {environment} from "../../../environments/environment";
import {Modal} from "bootstrap";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  email: string = environment.slaserEmail;
  telephone: string = environment.telephone;
  address: any = environment.address;
  instagramSlaser: string = environment.instagram;
  modal: Modal | null = null;

  constructor() {
  }

  ngOnInit(): void {
  }

  mailMe(){
    var mail = document.createElement("a");
    mail.href = "mailto:" + environment.slaserEmail + "?subject=" + environment.slaserEmailSubject + "&body=Hola Sonia, \n";
    mail.click();
  }
}

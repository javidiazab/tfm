"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CalendarComponent = void 0;
var core_1 = require("@angular/core");
var calendar_date_1 = require("../../model/calendar.date");
var Utils_1 = require("../../utils/Utils");
var Constants_1 = require("../../utils/Constants");
var CalendarComponent = /** @class */ (function () {
    function CalendarComponent(appointmentService, datePipe, router, activatedRoute) {
        this.appointmentService = appointmentService;
        this.datePipe = datePipe;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.title = 'Calendario';
        this.monthNumber = 0;
        this.selectMonth = "";
        this.selectYear = 0;
        this.appointments = [];
        this.calendarGrid = [];
        this.calendarRow1 = [];
        this.calendarRow2 = [];
        this.calendarRow3 = [];
        this.calendarRow4 = [];
        this.calendarRow5 = [];
        this.currentDate = new Date();
        this.date = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), 1);
        console.log("en el constructor la fecha actual es: " + this.date.toLocaleDateString());
        this.monthsDefinition = Constants_1.monthsDefinition;
        this.daysDefinition = Constants_1.daysDefinition;
    }
    CalendarComponent.prototype.ngOnInit = function () {
        this.setSelectorsToDate(this.date);
        this.loadCalendar(this.calendarGrid, this.date);
    };
    CalendarComponent.prototype.previousMonth = function () {
        var _this = this;
        // @ts-ignore
        console.log("month definition " + this.monthsDefinition.find(function (x) { return x.name == _this.selectMonth; }).id);
        // @ts-ignore
        this.date.setMonth(this.monthsDefinition.find(function (x) { return x.name == _this.selectMonth; }).id - 2); //it is extracted from monthsDefinition (month number + 1) so if we substract 2 we are alredy decreasing one implitidly
        console.log("next date " + this.date.getMonth());
        console.log("previous modificado" + this.date.toLocaleDateString());
        this.setSelectorsToDate(this.date);
        this.loadCalendar(this.calendarGrid, this.date);
    };
    CalendarComponent.prototype.nextMonth = function () {
        var _this = this;
        // @ts-ignore
        console.log("month definition " + this.monthsDefinition.find(function (x) { return x.name == _this.selectMonth; }).id);
        // @ts-ignore
        this.date.setMonth(this.monthsDefinition.find(function (x) { return x.name == _this.selectMonth; }).id); //it is extracted from monthsDefinition (month number + 1) so if we send the id to setMonth we are alredy increasing one implitidly
        console.log("next modificado " + this.date.toLocaleDateString());
        this.setSelectorsToDate(this.date);
        this.loadCalendar(this.calendarGrid, this.date);
    };
    CalendarComponent.prototype.changeMonth = function () {
        var _this = this;
        // @ts-ignore
        this.date.setMonth(this.monthsDefinition.find(function (x) { return x.name == _this.selectMonth; }).id - 1);
        this.date.setFullYear(this.selectYear);
        console.log("mes cambiado a " + this.date.toLocaleDateString());
        this.loadCalendar(this.calendarGrid, this.date);
    };
    CalendarComponent.prototype.changeYear = function () {
        var _this = this;
        this.date.setFullYear(this.selectYear);
        // @ts-ignore
        this.date.setMonth(this.monthsDefinition.find(function (x) { return x.name == _this.selectMonth; }).id - 1);
        this.loadCalendar(this.calendarGrid, this.date);
        console.log("fecha final: " + this.date.toLocaleDateString());
    };
    CalendarComponent.prototype.today = function () {
        this.date = new Date();
        this.setSelectorsToDate(this.date);
        console.log("despues en today " + this.date.toLocaleDateString());
        this.loadCalendar(this.calendarGrid, this.date);
    };
    CalendarComponent.prototype.setSelectorsToDate = function (currentDate) {
        // @ts-ignore
        this.selectMonth = this.monthsDefinition.find(function (x) { return x.id == currentDate.getMonth() + 1; }).name;
        this.selectYear = currentDate.getFullYear();
        console.log("año + mes seleccionado: " + this.selectYear + " " + this.selectMonth);
    };
    CalendarComponent.prototype.loadCalendar = function (calendarGrid, date) {
        var loadDate = new Date(date);
        console.log("************** " + (loadDate.getMonth() + 1) + " *********************************");
        console.log("loadDate antes de getFirstDayOfCalendar: " + loadDate.toLocaleDateString());
        var firstDayOfCalendar = this.getFirstDayOfCalendar(loadDate);
        calendarGrid = this.createCalendar(firstDayOfCalendar);
        this.loadAppointments(calendarGrid, calendarGrid[0].date, calendarGrid[calendarGrid.length - 1].date);
        this.calendarRow1 = calendarGrid.slice(0, 7);
        this.calendarRow2 = calendarGrid.slice(7, 14);
        this.calendarRow3 = calendarGrid.slice(14, 21);
        this.calendarRow4 = calendarGrid.slice(21, 28);
        this.calendarRow5 = calendarGrid.slice(28, 36);
    };
    CalendarComponent.prototype.getFirstDayOfCalendar = function (date) {
        var date1 = new Date(date);
        var day1Date = new Date(date1.getFullYear(), date1.getMonth(), 1); //TODO ver si esto hace falta todavia
        var dayOfWeek1 = this.getDayOfWeek(day1Date);
        if (dayOfWeek1 > 1) {
            date1.setDate(day1Date.getDate() - (dayOfWeek1 - 1));
        }
        else {
            date1 = day1Date;
        }
        console.log("el getFirstDayOfCalendar es: " + date1.toLocaleString());
        return date1;
    };
    CalendarComponent.prototype.getDayOfWeek = function (date) {
        var dayOfWeekDate = new Date(date);
        console.log("la entrada a getDayOfWeek es: " + dayOfWeekDate.toLocaleDateString());
        console.log("el dia de la semana que me dice que es (malo) es: " + dayOfWeekDate.getDay());
        // @ts-ignore
        var realday = this.daysDefinition.find(function (x) { return x.badDay === dayOfWeekDate.getDay(); }).realday; //TODO ver si esto es realmente necesario
        console.log("currentdate: " + this.currentDate.toLocaleDateString());
        console.log("date: " + this.date.toLocaleDateString());
        console.log("la salida da getDayOfWeek es: " + realday);
        return realday;
    };
    CalendarComponent.prototype.createCalendar = function (date) {
        var _this = this;
        var createCalDate = new Date(date);
        console.log("create calendar - 1vamos a trabajar con el dia: " + createCalDate.toLocaleDateString());
        console.log("y this.date es : " + this.date.toLocaleDateString());
        var calendarGrid = [];
        // @ts-ignore
        var currentMonth = this.monthsDefinition.find(function (x) { return x.name == _this.selectMonth; }).id - 1;
        createCalDate.setDate(createCalDate.getDate() - 1);
        console.log("create calendar - 2vamos a trabajar con el dia: " + createCalDate.toLocaleDateString());
        console.log("y this.date es : " + this.date.toLocaleDateString());
        for (var row = 0; row < 5; row++) {
            for (var col = 0; col < 7; col++) {
                createCalDate.setDate(createCalDate.getDate() + 1);
                calendarGrid.push(new calendar_date_1.CalendarDate(createCalDate.getDate(), row, col, createCalDate.getMonth() != currentMonth, createCalDate.toLocaleDateString() === this.currentDate.toLocaleDateString(), new Date(createCalDate.getFullYear(), createCalDate.getMonth(), createCalDate.getDate()), []));
            }
        }
        return calendarGrid;
    };
    CalendarComponent.prototype.loadAppointments = function (calendarGrid, beginning, end) {
        console.log("beginning: " + beginning.toLocaleDateString() + " end: " + end.toLocaleDateString());
        this.getAndLoadAppointmentList(calendarGrid, beginning, end);
    };
    CalendarComponent.prototype.getAndLoadAppointmentList = function (calendarGrid, startDate, endDate) {
        var _this = this;
        this.appointmentService
            .getAppointments(startDate, endDate)
            .subscribe(function (appointments) {
            _this.appointments = appointments;
            _this.appointments.forEach(function (x) {
                console.log("Lista appointments mes " + x.subject + x.startingDate);
            });
            calendarGrid.forEach(function (x) { return _this.loadParticularDay(x, _this.appointments); });
        });
        // (response) => this.onGetForecastResult(response.json()),
        //   (error) => this.onGetForecastError(error.json()),
        //   () => this.onGetForecastComplete()
    };
    CalendarComponent.prototype.loadParticularDay = function (calendarDate, appointments) {
        var apmtPerDay = appointments.filter(function (x) { return Utils_1.dateFormatDS(x.startingDate) === Utils_1.dateFormatDS(calendarDate.date); });
        calendarDate.appointments = apmtPerDay;
        console.log("apmPerDay: " + apmtPerDay.length);
        apmtPerDay.forEach(function (x) { return console.log("apmtPerDay.subject: " + x.subject + " " + x.startingDate); });
    };
    CalendarComponent.prototype.findApmt = function (calendarDate) {
        console.log("lista para el día " + calendarDate.date.toLocaleDateString());
        this.router.navigate(['/', 'appointments'], { queryParams: { calendarDate: JSON.stringify(calendarDate) } }); //{ queryParams: { date1: day, date2: day}});
    };
    CalendarComponent = __decorate([
        core_1.Component({
            selector: 'app-calendar',
            templateUrl: './calendar.component.html',
            styleUrls: ['./calendar.component.css']
        })
    ], CalendarComponent);
    return CalendarComponent;
}());
exports.CalendarComponent = CalendarComponent;

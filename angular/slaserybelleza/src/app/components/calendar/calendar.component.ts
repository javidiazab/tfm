import {Component, OnInit, ViewChild} from '@angular/core';
import {CalendarDate} from "../../model/calendar.date";
import {AppointmentService} from "../../services/appointment/appointment.service";
import {DatePipe} from '@angular/common';
import {Appointment} from "../../model/appointment";
import {Router, ActivatedRoute} from '@angular/router';
import {logout} from "../../utils/Utils";
import {monthsDefinition, daysDefinition, weekDays} from "../../utils/Constants";
import {HttpErrorResponse} from "@angular/common/http";
import {LoginComponent} from "../login/login.component";
import moment = require("moment");


@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {
  @ViewChild(LoginComponent) loginComponent: any;

  public title: string = 'Calendario';

  public date: Date;
  public currentDate: Date;
  public monthNumber: number = 0;

  public selectMonth: string = '';
  public selectYear: number = 0;
  public appointments: Appointment[] = [];

  public calendarGrid: CalendarDate[] = [];
  public calendarRow1: CalendarDate[] = [];
  public calendarRow2: CalendarDate[] = [];
  public calendarRow3: CalendarDate[] = [];
  public calendarRow4: CalendarDate[] = [];
  public calendarRow5: CalendarDate[] = [];

  public monthsDefinition;
  public weekDays;

  constructor(private appointmentService: AppointmentService,
              private datePipe: DatePipe, private router: Router, private activatedRoute: ActivatedRoute) {
    this.currentDate = new Date();
    this.date = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), 1);
    this.monthsDefinition = monthsDefinition;
    this.weekDays = weekDays;
  }

  ngOnInit() {
    this.setSelectorsToDate(this.date);
    this.loadCalendar(this.calendarGrid, this.date);
  }

  public previousMonth() {
    // @ts-ignore
    this.date.setMonth(this.monthsDefinition.find(x => x.name == this.selectMonth).id - 2); //it is extracted from monthsDefinition (month number + 1) so if we substract 2 we are alredy decreasing one implitidly
    this.setSelectorsToDate(this.date);
    this.loadCalendar(this.calendarGrid, this.date);
  }

  public nextMonth() {
    // @ts-ignore
    this.date.setMonth(this.monthsDefinition.find(x => x.name == this.selectMonth).id); //it is extracted from monthsDefinition (month number + 1) so if we send the id to setMonth we are alredy increasing one implitidly
    this.setSelectorsToDate(this.date);
    this.loadCalendar(this.calendarGrid, this.date);
  }

  public changeMonth() {
    // @ts-ignore
    this.date.setMonth(this.monthsDefinition.find(x => x.name == this.selectMonth).id - 1);
    this.date.setFullYear(this.selectYear);
    this.loadCalendar(this.calendarGrid, this.date);
  }

  public changeYear() {
    this.date.setFullYear(this.selectYear);
    // @ts-ignore
    this.date.setMonth(this.monthsDefinition.find(x => x.name == this.selectMonth).id - 1);
    this.loadCalendar(this.calendarGrid, this.date);
  }

  public today() {
    this.date = new Date();
    this.setSelectorsToDate(this.date);
    this.loadCalendar(this.calendarGrid, this.date);
  }

  private setSelectorsToDate(currentDate: Date) {
    // @ts-ignore
    this.selectMonth = this.monthsDefinition.find(x => x.id == currentDate.getMonth() + 1).name;
    this.selectYear = currentDate.getFullYear();
  }

  private loadCalendar(calendarGrid: CalendarDate[], date: Date) {
    let loadDate: Date = new Date(date);
    let firstDayOfCalendar: Date = this.getFirstDayOfCalendar(loadDate);
    calendarGrid = this.createCalendar(firstDayOfCalendar);

    this.loadAppointments(calendarGrid, calendarGrid[0].date, calendarGrid[calendarGrid.length - 1].date);

    this.calendarRow1 = calendarGrid.slice(0, 7);
    this.calendarRow2 = calendarGrid.slice(7, 14);
    this.calendarRow3 = calendarGrid.slice(14, 21);
    this.calendarRow4 = calendarGrid.slice(21, 28);
    this.calendarRow5 = calendarGrid.slice(28, 36);
  }

  public findApmt(calendarDate: CalendarDate) {
    this.router.navigate(['/', 'appointments'],
      {queryParams: {calendarDate: JSON.stringify(calendarDate)}}); //{ queryParams: { date1: day, date2: day}});
  }

  private getFirstDayOfCalendar(date: Date): Date {
    let date1: Date = new Date(date);
    let day1Date: Date = new Date(date1.getFullYear(), date1.getMonth(), 1);  //TODO ver si esto hace falta todavia
    let dayOfWeek1 = this.getDayOfWeek(day1Date);
    dayOfWeek1 > 1 ?
      date1.setDate(day1Date.getDate() - (dayOfWeek1 - 1)) :
      date1 = day1Date;
    return date1;
  }

  private getDayOfWeek(date: Date): number {
    let dayOfWeekDate: Date = new Date(date);
    // @ts-ignore
    let realday = daysDefinition.find(x => x.badDay === dayOfWeekDate.getDay()).realday;  //TODO ver si esto es realmente necesario
    return realday;
  }

  private createCalendar(date: Date): CalendarDate[] {
    let createCalDate: Date = new Date(date);
    let calendarGrid: CalendarDate[] = [];
    // @ts-ignore
    let currentMonth = this.monthsDefinition.find(x => x.name == this.selectMonth).id - 1;
    createCalDate.setDate(createCalDate.getDate() - 1);
    for (let row = 0; row < 5; row++) {
      for (let col = 0; col < 7; col++) {
        createCalDate.setDate(createCalDate.getDate() + 1);
        calendarGrid.push(new CalendarDate(
          createCalDate.getDate(), row, col, createCalDate.getMonth() != currentMonth,
          createCalDate.toLocaleDateString() === this.currentDate.toLocaleDateString(),
          new Date(createCalDate.getFullYear(), createCalDate.getMonth(), createCalDate.getDate()), []));
      }
    }
    return calendarGrid;
  }

  private loadAppointments(calendarGrid: CalendarDate[], beginning: Date, end: Date) {
    console.log("beginning: " + beginning.toLocaleDateString() + " end: " + end.toLocaleDateString());
    this.getAndLoadAppointmentList(calendarGrid, beginning, end);
  }

  private getAndLoadAppointmentList(calendarGrid: CalendarDate[], startDate: Date, endDate: Date): void {
    this.appointmentService
      .getAppointments(startDate, endDate)
      .subscribe(result => {
          if (result instanceof HttpErrorResponse) {
            logout();
            this.router.navigate(['/', 'home']);
          } else {
            this.appointments = result;
            // this.appointments.forEach(x => {
            //   console.log("List appointments month ", x)
            // });
            calendarGrid.forEach(x => this.loadParticularDay(x, this.appointments));
          }
        },
        (error) => console.log("Error getting appointments", error),
        () => console.log("complete getting appointments"));
  }

  private loadParticularDay(calendarDate: CalendarDate, appointments: Appointment[]) {
    let apmtPerDay: Appointment[] = appointments.filter(
      x => moment(x.startingDate).format('YYYY-MM-DD') === moment(calendarDate.date).format('YYYY-MM-DD'));

    calendarDate.appointments = apmtPerDay;
    apmtPerDay.forEach(x => console.log("apmtPerDay.subject: ", x));
  }

  // addClass() {
  //   //this.myButton.nativeElement.classList.add("my-class"); //BAD PRACTICE
  //   //this.renderer.addClass(this.cal.nativeElement, "my-class");
  // }
  //
  // removeClass() {
  //   //this.myButton.nativeElement.classList.remove("my-class"); //BAD PRACTICE
  //   //this.renderer.removeClass(this.cal.nativeElement, "my-class");
  // }
  //
  //
}


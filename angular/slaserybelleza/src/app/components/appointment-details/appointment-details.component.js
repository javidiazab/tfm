"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppointmentDetailsComponent = void 0;
var core_1 = require("@angular/core");
var appointment_1 = require("../../model/appointment");
var Utils_1 = require("../../utils/Utils");
var Constants_1 = require("../../utils/Constants");
var AppointmentDetailsComponent = /** @class */ (function () {
    function AppointmentDetailsComponent(location, appointmentService) {
        this.location = location;
        this.appointmentService = appointmentService;
        this.title = "Cita";
        // if(!this.appointment) {
        this.appointment = new appointment_1.Appointment("", "", "", "1 hora", "Servicio", new Date(), new Date());
        this.durationSelect = Constants_1.durationSelect;
        this.typeSelect = Constants_1.typeSelect;
    }
    AppointmentDetailsComponent.prototype.ngOnInit = function () {
        console.log("en appointment details--------------------------------------------------------------");
        console.log("en " + this.appointment.startingDate.getMonth());
        console.log("appointment: " + JSON.stringify(this.appointment));
        this.stDate = Utils_1.dateFormatDS(this.appointment.startingDate);
        this.stTime = Utils_1.TimeFormatDS(this.appointment.startingDate);
        this.ndDate = Utils_1.dateFormatDS(this.appointment.startingDate);
        this.ndTime = Utils_1.TimeFormatDS(this.appointment.startingDate);
    };
    AppointmentDetailsComponent.prototype.onSubmit = function () {
        var _this = this;
        this.appointment.startingDate = new Date(this.stDate);
        this.appointment.startingDate.setHours(Number(this.stTime.slice(0, 2)), Number(this.stTime.slice(3, 6)));
        this.appointment.endDate = new Date(this.ndDate);
        this.appointment.endDate.setHours(Number(this.ndTime.slice(0, 2)), Number(this.ndTime.slice(3, 6)));
        console.log("vamos a dar de alta: " + this.appointment.startingDate.toISOString() + "xxx" + this.appointment.endDate.toISOString());
        //this.appointment.beginDate.setFullYear(this.starting.) = this.starting.date;
        console.log("en appointment details antes de llamar al servicio para dar de alta" + " subject: " + this.appointment.subject
            + " place: " + this.appointment.place + " description: " + this.appointment.description + " duration: "
            + this.appointment.duration + " beginDate: " + this.appointment.startingDate
            + " endDate: " + this.appointment.endDate);
        this.appointmentService
            .addAppointment(this.appointment)
            .subscribe(function (appointment) { return _this.appointment = appointment; }, function (error) { return console.log("Error adding new appointment"); }, function () { return console.log("complete adding new appointment"); });
        console.log("Nuevo Id:" + this.appointment.id);
        this.location.back();
    };
    AppointmentDetailsComponent.prototype.back = function () {
        this.location.back();
    };
    __decorate([
        core_1.Input()
    ], AppointmentDetailsComponent.prototype, "appointment");
    AppointmentDetailsComponent = __decorate([
        core_1.Component({
            selector: 'app-appointment-details',
            templateUrl: './appointment-details.component.html',
            styleUrls: ['./appointment-details.component.css']
        })
    ], AppointmentDetailsComponent);
    return AppointmentDetailsComponent;
}());
exports.AppointmentDetailsComponent = AppointmentDetailsComponent;

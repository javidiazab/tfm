import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Location} from '@angular/common'
import {Router} from '@angular/router';
import {Appointment} from "../../model/appointment";
import {AppointmentService} from "../../services/appointment/appointment.service";
import {logout} from "../../utils/Utils";
import {durationSelect, typeSelect} from "../../utils/Constants";
import {FormGroup, FormBuilder, Validators, AbstractControl} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import moment = require("moment");


@Component({
  selector: 'app-appointment-details',
  templateUrl: './appointment-details.component.html',
  styleUrls: ['./appointment-details.component.css']
})
export class AppointmentDetailsComponent implements OnInit {
  @Input() appointment: Appointment;
  @Output() updateAppointment = new EventEmitter<Appointment>()  //this.onSave.emit();
  public durationSelect;
  public typeSelect;
  public detailsForm: FormGroup = new FormGroup({});
  public saved: boolean = false;
  validationMessages = {
    'subject': {
      'required': 'La cita no es válida',
      'minlength': 'Longitud incorrecta. 3 caracteres al menos'
    },
    'place': {'required': 'El lugar no es válido'},
    'duration': {'required': 'La duración no es válida'},
    'type': {'required': 'El tipo no es válido'},
    'cancel': {'required': 'Cencelación no válida'},
    'startingDate': {'required': 'Fecha comienzo inválida'},
    'startingTime': {'required': 'Hora comienzo inválida'},
    'endDate': {'required': 'Fecha final inválida'},
    'endTime': {'required': 'Hora final inválida'},
    'datesGroup': {
      'incoherentDates': 'Fecha de inicio mayor que fecha final',
      'incoherentTimes': 'Hora de inicio mayor que hora final'
    }
  };

  formErrors = {
    'subject': '', 'place': '', 'duration': '', 'type': '', 'cancel': '', 'startingDate': '',
    'startingTime': '', 'endDate': '', 'endTime': '', 'datesGroup': ''
  };

  constructor(private location: Location, private appointmentService: AppointmentService,
              private fb: FormBuilder, private router: Router) {
    this.durationSelect = durationSelect;
    this.typeSelect = typeSelect;
    this.appointment = new Appointment("", "", "", "1 hora",
      "Servicio", new Date(), new Date());

    //examples
    // this.detailsForm.get('subject')?.valueChanges.subscribe(value => {console.log("valor cambia", this.detailsForm.get('subject')?.value)})
    // this.detailsForm.get('datesGroup')?.valueChanges.subscribe(value => {console.log("valor cambia", this.detailsForm.get('datesGroup'))})
    // this.detailsForm?.valueChanges.subscribe(value => {console.log("valor cambia", JSON.stringify(this.detailsForm?.value))})

    //using new FormGroup
    // this.detailsForm = new FormGroup({
    //   subject: new FormControl('', Validators.required),
    //   place: new FormControl('', [Validators.required]),
    //   description: new FormControl(),
    //   duration: new FormControl('', [Validators.required]),
    //   type: new FormControl('', [Validators.required]),
    //   cancel: new FormControl(),
    //   datesGroup: new FormGroup({
    //     startingDate: new FormControl('', [Validators.required]),
    //     startingTime: new FormControl('', [Validators.required]),
    //     endDate: new FormControl('', [Validators.required]),
    //     endTime: new FormControl('', [Validators.required])
    //   }, {validators: dateValidator})
    // });
  }

  ngOnInit() {
    this.detailsForm = this.fb.group({
      subject: [this.appointment.subject, [Validators.required, Validators.minLength(3)]],
      place: [this.appointment.place, [Validators.required]],
      description: [this.appointment.description],
      duration: [this.appointment.duration, [Validators.required]],
      type: [this.appointment.type, [Validators.required]],
      cancel: [!!this.appointment.cancelDate],
      datesGroup: this.fb.group({
        startingDate: [moment(this.appointment.startingDate).format('YYYY-MM-DD'), [Validators.required]],
        startingTime: [moment(this.appointment.startingDate).format('HH:mm'), [Validators.required]],
        endDate: [moment(this.appointment.endDate).format('YYYY-MM-DD'), [Validators.required]],
        endTime: [moment(this.appointment.endDate).format('HH:mm'), [Validators.required]]
      }, {validators: twoDateTimesValidator})
    });

    this.detailsForm.valueChanges.subscribe(data => {
      this.logValidationErrors(this.detailsForm);
    });
  }

  logValidationErrors(group: FormGroup = this.detailsForm): void {
    Object.keys(group.controls).forEach((key: string) => {
      const abstractControl = group.get(key);
      // @ts-ignore
      this.formErrors[key] = '';
      if (abstractControl && !abstractControl.valid && (abstractControl.touched || abstractControl.dirty)) {
        // @ts-ignore
        const messages = this.validationMessages[key];
        for (const errorKey in abstractControl.errors) {
          if (errorKey) {
            // @ts-ignore
            this.formErrors[key] += messages[errorKey] + ' ';
          }
        }
      }

      if (abstractControl instanceof FormGroup) {
        this.logValidationErrors(abstractControl);
        //abstractControl?.disable();
        //console.log('key = ' + key + ' value = ' + abstractControl);
        //abstractControl?.disable();
        //abstractControl?.markAsDirty();
      }
    });
  }

  onSubmit(): void {
    this.appointment.subject = this.subject?.value;
    this.appointment.place = this.place?.value;
    this.appointment.description = this.description?.value;
    this.appointment.duration = this.duration?.value;
    this.appointment.type = this.type?.value;
    this.appointment.cancelDate = this.cancel?.value ? new Date() : undefined;
    this.appointment.startingDate = new Date(this.startingDate?.value);
    this.appointment.startingDate.setHours(Number(this.startingTime?.value.slice(0, 2)), Number(this.startingTime?.value.slice(3, 6)));
    this.appointment.endDate = new Date(this.endDate?.value);
    this.appointment.endDate.setHours(Number(this.endTime?.value.slice(0, 2)), Number(this.endTime?.value.slice(3, 6)));
    if (this.detailsForm.valid) {
      this.save(this.appointment);
    } else {
      alert("Error validating form")
    }
  }

  private save(appointment: Appointment) {
    this.appointmentService
      .addAppointment(appointment)
      .subscribe(result => {
          if (result instanceof HttpErrorResponse) {
            logout();
            this.router.navigate(['/', 'home']);
          } else {
            this.appointment = result;
            this.updateAppointment.emit(this.appointment);
            this.saved = true;
            //this.openModal();
          }
        },
        (error) => console.log("Error saving appointment"),
        () => console.log("Complete saving appointment")
      );
  }

  get subject() {
    return this.detailsForm.get('subject');
  }

  get place() {
    return this.detailsForm.get('place');
  }

  get description() {
    return this.detailsForm.get('description');
  }

  get duration() {
    return this.detailsForm.get('duration');
  }

  get type() {
    return this.detailsForm.get('type');
  }

  get cancel() {
    return this.detailsForm.get('cancel');
  }

  get startingDate() {
    return this.detailsForm.get('datesGroup')?.get('startingDate');
  }

  get startingTime() {
    return this.detailsForm.get('datesGroup')?.get('startingTime');
  }

  get endDate() {
    return this.detailsForm.get('datesGroup')?.get('endDate');
  }

  get endTime() {
    return this.detailsForm.get('datesGroup')?.get('endTime');
  }

  get datesGroup() {
    return this.detailsForm.get('datesGroup');
  }

  onFormClick() {
    this.saved = false;
  }

  back(): void {
    this.location.back()
  }

//   get f () { return this.detailsForm.controls}
//
//   validateDateTimes(stDatelit: string, stTimelit: string, ndDatelit: string, ndTimelit: string) {
//     console.log("validando validateDateTimes");
//     return (formGroup: FormGroup) => {
//       const startingDate = formGroup.controls[stDatelit];
//       const startingTime = formGroup.controls[stTimelit];
//       const endDate = formGroup.controls[ndDatelit];
//       const endTime = formGroup.controls[ndTimelit];
//       console.log("validando validateDateTimes - " );
//
//       let dateTime1 = dateFormatSddmmyyyyhhssD(startingDate.value, startingTime.value);
//       let dateTime2 = dateFormatSddmmyyyyhhssD(endDate.value, endTime.value);
//
//       if (dateTime1 <= dateTime2 || endDate.pristine || endTime.pristine) {
//         return null;
//       } else {
//         return endDate.setErrors({'incoherentDates': true});
//       }
//     }
}

//
//   static matchDates(abstractControl: AbstractControl) {
//     let date1 = abstractControl.get('startingDate')?.value;
//     let date2 = abstractControl.get('endDate')?.value;
//     console.log("en matchDates date1", date1);
//     console.log("en matchDates date2", date2);
//     if (date1 != date2) {
//       abstractControl.get('endDate')?.setErrors({
//         incoherentDates: true
//       });
//       return {incoherentDates: true};   //esto para que devuelva algo
//     } else {
//       return null;
//     }
//   }
// }

//export const twoDateTimesValidator: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
export function twoDateTimesValidator(group: AbstractControl): { [key: string]: any } | null {
  let stDate = group.get('startingDate')?.value;
  let ndDate = group.get('endDate')?.value;
  let stTime = group.get('startingTime')?.value;
  let ndTime = group.get('endTime')?.value;

  if (stDate && ndDate && stTime && ndTime) {
    if (stDate > ndDate) {
      return {'incoherentDates': true};
    }

    let begining = moment(stDate + " " + stTime, 'DD-MM-YYYY HH:mm');
    let end = moment(ndDate + " " + ndTime, 'DD-MM-YYYY HH:mm');
    if (begining.isAfter(end)) { // || group.get('endDate')?.pristine || group.get('endTime')?.pristine) {
      return {'incoherentTimes': true};
    }
  }
  return null;
  //return dateTime2 > dateTime1 ? { datesValidation: true } : null;
}


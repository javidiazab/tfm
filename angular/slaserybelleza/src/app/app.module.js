"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppModule = void 0;
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms"); // <-- NgModel lives here
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
var add_appointment_component_1 = require("./components/add-appointment/add-appointment.component");
var appointment_details_component_1 = require("./components/appointment-details/appointment-details.component");
var appointments_list_component_1 = require("./components/appointments-list/appointments-list.component");
var calendar_component_1 = require("./components/calendar/calendar.component");
var home_component_1 = require("./components/home/home.component");
var companyshome_component_1 = require("./components/companyshome/companyshome.component");
var http_1 = require("@angular/common/http");
var login_component_1 = require("./components/login/login.component");
var contact_component_1 = require("./components/contact/contact.component");
var common_1 = require("@angular/common");
var footer_component_1 = require("./components/footer/footer.component");
var navbar_component_1 = require("./components/navbar/navbar.component");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                add_appointment_component_1.AddAppointmentComponent,
                appointment_details_component_1.AppointmentDetailsComponent,
                appointments_list_component_1.AppointmentsListComponent,
                calendar_component_1.CalendarComponent,
                home_component_1.HomeComponent,
                companyshome_component_1.CompanyshomeComponent,
                login_component_1.LoginComponent,
                contact_component_1.ContactComponent,
                footer_component_1.FooterComponent,
                navbar_component_1.NavbarComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                app_routing_module_1.AppRoutingModule,
                http_1.HttpClientModule,
                forms_1.FormsModule
            ],
            providers: [common_1.DatePipe],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;

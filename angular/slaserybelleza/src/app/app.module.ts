import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'; // <-- NgModel lives here
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AddAppointmentComponent} from './components/add-appointment/add-appointment.component';
import {AppointmentDetailsComponent} from './components/appointment-details/appointment-details.component';
import {AppointmentsListComponent} from './components/appointments-list/appointments-list.component';
import {CalendarComponent} from './components/calendar/calendar.component';
import {HomeComponent} from './components/home/home.component';
import {CompanyshomeComponent} from './components/companyshome/companyshome.component';
import {HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './components/login/login.component';
import {ContactComponent} from './components/contact/contact.component';
import {DatePipe} from '@angular/common';
import {FooterComponent} from './components/footer/footer.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {NavbarcompanyComponent} from './components/navbarcompany/navbarcompany.component';
import {AboutUsComponent} from './components/about-us/about-us.component';

@NgModule({
  declarations: [
    AppComponent,
    AddAppointmentComponent,
    AppointmentDetailsComponent,
    AppointmentsListComponent,
    CalendarComponent,
    HomeComponent,
    CompanyshomeComponent,
    LoginComponent,
    ContactComponent,
    FooterComponent,
    NavbarComponent,
    NavbarcompanyComponent,
    AboutUsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }

import {Appointment} from "./appointment";

export class CalendarDate {
  day: number;
  row: number;
  col: number;
  enabled: boolean;
  today: boolean;
  date: Date;
  appointments: Appointment[];

  constructor(day: number,
              row: number,
              col: number,
              enabled: boolean,
              today: boolean,
              date: Date,
              appointments: Appointment[]) {
    this.day = day;
    this.row = row;
    this.col = col;
    this.enabled = enabled;
    this.today = today;
    this.date = date;
    this.appointments = appointments;
  }
}


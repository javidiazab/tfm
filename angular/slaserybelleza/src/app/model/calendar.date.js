"use strict";
exports.__esModule = true;
exports.CalendarDate = void 0;
var CalendarDate = /** @class */ (function () {
    function CalendarDate(day, row, col, enabled, today, date, appointments) {
        this.day = day;
        this.row = row;
        this.col = col;
        this.enabled = enabled;
        this.today = today;
        this.date = date;
        this.appointments = appointments;
    }
    return CalendarDate;
}());
exports.CalendarDate = CalendarDate;

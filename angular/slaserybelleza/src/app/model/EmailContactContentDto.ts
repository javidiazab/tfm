export class  EmailContactContentDto {
  name?: string;
  surname?: string;
  telephone?: string;
  email?: string;
  comment?: string;

  constructor(name?: string, surname?: string, telephone?: string, email?: string, comment?: string) {
    name ? this.name = name : name = "";
    surname ? this.surname = surname : surname = "";
    telephone ? this.telephone = telephone : telephone = "";
    email ? this.email = email : email = "";
    comment ? this.comment = comment : comment = "";
  }
}

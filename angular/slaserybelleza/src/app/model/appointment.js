"use strict";
exports.__esModule = true;
exports.Appointment = void 0;
var Appointment = /** @class */ (function () {
    // constructor(...args: any[]) {
    //   // id: number, idCli: number, subject: string, place: string, description: string,
    //   //           duration: string, type: string, startingDate: Date, endDate: Date, cancelDate: Date,
    //   //           creationDate: Date, usuMod: string
    //   if (args.length === 12) {
    //     this.id = args[0];
    //     this.idCli = args[1];
    //     this.subject = args[2];
    //     this.place = args[3];
    //     this.description = args[4];
    //     this.duration = args[5];
    //     this.type = args[6];
    //     this.startingDate = args[7];
    //     this.endDate = args[8];
    //     this.cancelDate = args[9];
    //     this.creationDate = args[10];
    //     this.usuMod = args[11];
    //   } else {
    //     this.id = undefined;
    //     this.idCli = undefined;
    //     this.subject = '';
    //     this.place = '';
    //     this.description = undefined;
    //     this.duration = '';
    //     this.type = '';
    //     this.startingDate = new Date;
    //     this.endDate = new Date;
    //     this.cancelDate = undefined;
    //     this.creationDate = undefined;
    //     this.usuMod = undefined;
    //   }
    // }
    function Appointment(subject, place, description, duration, type, startingDate, endDate, idCli, id, cancelDate, creationDate, usuMod) {
        if (id) {
            this.id = id;
        }
        if (idCli) {
            this.idCli = idCli;
        }
        this.subject = subject;
        this.place = place;
        if (subject) {
            this.description = description;
        }
        this.duration = duration;
        this.type = type;
        this.startingDate = startingDate;
        this.endDate = endDate;
        if (cancelDate) {
            this.cancelDate = cancelDate;
        }
        if (creationDate) {
            this.creationDate = creationDate;
        }
        if (usuMod) {
            this.usuMod = usuMod;
        }
    }
    return Appointment;
}());
exports.Appointment = Appointment;

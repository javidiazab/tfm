export class AppointmentDto {
  id?: number | undefined;
  idCli?: number | undefined;
  subject: string;
  place: string;
  description?: string | undefined;
  duration: string;
  type: string;
  startingDate: string;
  endDate: string;
  cancelDate?: string | undefined;
  usuMod?: string | undefined;

  // constructor(...args: any[]) {
  //   // id: number, idCli: number, subject: string, place: string, description: string,
  //   //           duration: string, type: string, startingDate: Date, endDate: Date, cancelDate: Date,
  //   //           creationDate: Date, usuMod: string
  //   if (args.length === 12) {
  //     this.id = args[0];
  //     this.idCli = args[1];
  //     this.subject = args[2];
  //     this.place = args[3];
  //     this.description = args[4];
  //     this.duration = args[5];
  //     this.type = args[6];
  //     this.startingDate = args[7];
  //     this.endDate = args[8];
  //     this.cancelDate = args[9];
  //     this.creationDate = args[10];
  //     this.usuMod = args[11];
  //   } else {
  //     this.id = undefined;
  //     this.idCli = undefined;
  //     this.subject = '';
  //     this.place = '';
  //     this.description = undefined;
  //     this.duration = '';
  //     this.type = '';
  //     this.startingDate = new Date;
  //     this.endDate = new Date;
  //     this.cancelDate = undefined;
  //     this.creationDate = undefined;
  //     this.usuMod = undefined;
  //   }
  // }

  constructor(subject: string, place: string, description: string | undefined, duration: string, type: string,
              startingDate: string, endDate: string, idCli: number | undefined, id: number | undefined,
              cancelDate: string | undefined, usuMod: string | undefined) {
    this.id = id;
    this.idCli = idCli;
    this.subject = subject;
    this.place = place;
    this.description = description;
    this.duration = duration;
    this.type = type;
    this.startingDate = startingDate;
    this.endDate = endDate;
    this.cancelDate = cancelDate;
    this.usuMod = usuMod;
  }
}

"use strict";
exports.__esModule = true;
exports.EmailContactContentDto = void 0;
var EmailContactContentDto = /** @class */ (function () {
    function EmailContactContentDto(name, surname, telephone, email, comment) {
        name ? this.name = name : name = "";
        surname ? this.surname = surname : surname = "";
        telephone ? this.telephone = telephone : telephone = "";
        email ? this.email = email : email = "";
        comment ? this.comment = comment : comment = "";
    }
    return EmailContactContentDto;
}());
exports.EmailContactContentDto = EmailContactContentDto;

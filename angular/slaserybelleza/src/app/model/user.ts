export class User {
  id?: number | undefined;
  user: string;
  password: string;
  rol: string | undefined;
  retries: number;
  creationDate?: Date | undefined;
  endDate?: Date;

  constructor( user: string, password: string, rol: string, retries: number,
               id: number, creationDate?: Date, endDate?: Date) {
    if (id) {
      this.id = id;
    }
    this.user = user;
    this.password = password;
    this.rol = rol;
    this.retries = retries;
    if (creationDate) {
      this.creationDate = creationDate;
    }
    if (endDate) {
      this.endDate = endDate;
    }
  }
}

import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, of} from "rxjs";
import {Appointment} from "../../model/appointment";
import {catchError, tap} from "rxjs/operators";
import {DatePipe} from '@angular/common';
import {map} from 'rxjs/operators';
import {environment} from "../../../environments/environment";
import moment = require("moment");
import {AppointmentDto} from "../../model/appointmentDto";

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {
  baseUrl: string;

  constructor(private httpClient: HttpClient, private datePipe: DatePipe) {
    this.baseUrl = environment.sLaserUrl + environment.contextPath + environment.appointmentUrl;
  }

  /** POST: add a new appointment to the server */
  public addAppointment(appointment: Appointment): Observable<Appointment> {
    const url = `${this.baseUrl + environment.saveUrl}`;
    console.log("Call to " + url);

    return this.httpClient.post<AppointmentDto>(url, mapToAppointmentDto(appointment), {headers: this.getHeaders()}).pipe(
      map(appointmentDto => {
        return mapToAppointment(appointmentDto);
      }),
      tap((newAppointment: Appointment) => console.log(`added appointent w/ id=${newAppointment.id}`)),
      catchError(this.handleError<Appointment>('addAppointment'))
    );
  }

  /** GET appointment by id. Will issue 404 if id not found */
  public getAppointment(id: number): Observable<Appointment> {
    const url = `${this.baseUrl}/${id}`;
    return this.httpClient.get<AppointmentDto>(url, {headers: this.getHeaders()}).pipe(
      map(appointmentDto => {
        return mapToAppointment(appointmentDto);
      }),
      tap(_ => console.log(`fetched appointment id=${id}`)),
      catchError(this.handleError<Appointment>(`getAppointment id=${id}`))
    );
  }

  /** GET delete appointment by id. Will issue 404 if id not found */
  public deleteAppointment(id: number): Observable<Appointment> { //TODO should return boolean
    if (id && !isNaN(id)) {
      const url = `${this.baseUrl + environment.deleteUrl}${id}`;
      return this.httpClient.get<Appointment>(url, {headers: this.getHeaders()}).pipe(
        tap(_ => console.log(`deleted appointment id=${id}`)),
        catchError(this.handleError<Appointment>(`deleteAppointment id=${id}`))
      );
    } else {
      console.log("Delete by id. ID not assigned");
      return new Observable<Appointment>();
    }
  }

  /** POST list appointments from the server */
  public getAppointments = (begining: Date, end: Date): Observable<Appointment[]> => {
    const url = `${this.baseUrl + environment.listUrl}?start=${this.datePipe.transform(begining, "yyyy-MM-dd")}&end=${this.datePipe.transform(end, "yyyy-MM-dd")}`;
    return this.httpClient.post<AppointmentDto[]>(url, null, {headers: this.getHeaders()})
      .pipe(
        map(newAppointmentDtoList => {
          let appointments: Appointment[] = [];
          newAppointmentDtoList.forEach(x => {
            appointments.push(mapToAppointment(x))
          });
          appointments.forEach(x => console.log("recovered: ", x.startingDate));
          return appointments;
        }),
        tap((newAppointmentList: Appointment[]) => {
          newAppointmentList.forEach(x => {
            console.log("tap is: " + x.startingDate  + " " + x.creationDate);
          });
        }),
        catchError(this.handleError<Appointment[]>('Error in AppointmentsList'))
      );
  }

  /** POST list appointments by idCli from the server */
  public cliIdAppointments = (idCli: number, begining: Date, end: Date): Observable<Appointment[]> => {
    const url = `${this.baseUrl + environment.listUrl}/${idCli}?start=${begining.getFullYear()}-${begining.getMonth()}-${begining.getDate()}&end=${end.getFullYear()}-${end.getMonth()}-${end.getDate()}`;
    return this.httpClient.post<AppointmentDto[]>(url, null, {headers: this.getHeaders()})
      .pipe(
        map(newAppointmentDtoList => {
          let appointments: Appointment[] = [];
          newAppointmentDtoList.forEach(x => appointments.push(mapToAppointment(x)));
          return appointments;
        }),
        tap((newAppointmentList: Appointment[]) => console.log("First appointment by idCli: " + newAppointmentList[0])),
        catchError(this.handleError<Appointment[]>('AppointmentsListbyId'))
      );
  }

  private handleError<T>(operation = 'operation') {
    return (error: any): Observable<T> => {
      if(error instanceof HttpErrorResponse) {
        if (error.status == 500) {
          alert('Error de servidor');
        }
        if (error.status == 401) {
          alert('Error de autorización');
        }
        if (error.status == 403) {
          alert('Error de acceso');
        }
      }
      console.error(`${operation} failed: ${error.status} ${error.message}`);
      return of(error as T);
    };
  }
  private getHeaders(): HttpHeaders {
    return new HttpHeaders()
      .set('content-type', 'application/json')
      .set('Access-Control-Allow-Origin', '*')
      .set('Authorization', 'Bearer ' + localStorage.getItem('token'));
  }
}

export const headers = (): HttpHeaders => {
  return new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*')
    .set('Authorization', 'Bearer ' + localStorage.getItem('token'));
}

export const mapToAppointmentDto = (appointment: Appointment): AppointmentDto => {
  console.log("Mapeando: ", appointment)
  return new AppointmentDto(
    appointment.subject,
    appointment.place,
    appointment.description ? appointment.description : undefined,
    appointment.duration,
    appointment.type,
    moment(appointment.startingDate).format("YYYY-MM-DD HH:mm"),
    moment(appointment.endDate).format("YYYY-MM-DD HH:mm"),
    appointment.idCli ? appointment.idCli : undefined,
    appointment.id ? appointment.id : undefined,
    appointment.cancelDate ? moment(appointment.cancelDate).format("YYYY-MM-DD HH:mm") : undefined,
    appointment.usuMod ? appointment.usuMod : undefined);
}

export const mapToAppointment = (appointmentDto: AppointmentDto): Appointment => {
  console.log("Mapeando: ", appointmentDto)
  return new Appointment(
    appointmentDto.subject,
    appointmentDto.place,
    appointmentDto.description,
    appointmentDto.duration,
    appointmentDto.type,
    moment(appointmentDto.startingDate, "YYYY-MM-DD HH:mm").toDate(),
    moment(appointmentDto.endDate, "YYYY-MM-DD HH:mm").toDate(),
    appointmentDto.idCli,
    appointmentDto.id,
    appointmentDto.cancelDate ? moment(appointmentDto.cancelDate, "YYYY-MM-DD HH:mm").toDate() : undefined,
    undefined,
    appointmentDto.usuMod);
}


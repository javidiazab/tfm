"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppointmentService = void 0;
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var global_1 = require("../global");
var Utils_1 = require("../../utils/Utils");
var operators_2 = require("rxjs/operators");
var AppointmentService = /** @class */ (function () {
    function AppointmentService(httpClient, datePipe) {
        var _this = this;
        this.httpClient = httpClient;
        this.datePipe = datePipe;
        this.basicHeaders = new http_1.HttpHeaders()
            .set('content-type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', "'Basic ' " + btoa("admin" + ":" + "admin"));
        // .set('Authorization', "'Basic ' " + btoa(LoginComponent.usu + ":" + LoginComponent.password));
        this.httpOptions = { headers: this.basicHeaders };
        /** POST list appointments from the server */
        this.getAppointments = function (begining, end) {
            console.log("usu: " + "xxx" + " " + _this.httpOptions);
            var url = _this.baseUrl + global_1.Global.listUrl + "?start=" + _this.datePipe.transform(begining, "yyyy-MM-dd") + "&end=" + _this.datePipe.transform(end, "yyyy-MM-dd");
            return _this.httpClient.post(url, null, _this.httpOptions)
                .pipe(operators_2.map(function (newAppointmentList) {
                newAppointmentList.forEach(function (x) { return _this.fixDates(x); });
                return newAppointmentList;
            }), operators_1.tap(function (newAppointmentList) {
                newAppointmentList.forEach(function (x) {
                    console.log("starting en el tap es: " + x.startingDate + " " + x.creationDate); //TODO why it doesn't convert creationDate?
                });
                console.log("La primera cita es3: " + JSON.stringify(newAppointmentList[0]));
            }), operators_1.catchError(_this.handleError('Error in AppointmentsList')));
        };
        /** POST list appointments by idCli from the server */
        this.cliIdAppointments = function (idCli, begining, end) {
            console.log("Parametros deobtencio de la lista: " + begining.toLocaleDateString() + " y " + end.toLocaleDateString());
            var url = _this.baseUrl + global_1.Global.listUrl + "/" + idCli + "?start=" + begining.getFullYear() + "-" + begining.getMonth() + "-" + begining.getDate() + "&end=" + end.getFullYear() + "-" + end.getMonth() + "-" + end.getDate();
            return _this.httpClient.post(url, null, _this.httpOptions)
                .pipe(operators_1.tap(function (newAppointmentList) { return console.log("La primera cita por idCli es: " + newAppointmentList[0]); }), operators_1.catchError(_this.handleError('AppointmentsListbyId')));
        };
        this.baseUrl = global_1.Global.sLaserUrl + global_1.Global.appointmentUrl;
    }
    /** POST: add a new appointment to the server */
    AppointmentService.prototype.addAppointment = function (appointment) {
        var _this = this;
        var url = "" + (this.baseUrl + global_1.Global.saveUrl);
        console.log("Vamos a llamar a esta URL: " + url);
        console.log("en appointment service antes de llamar al post para dar de alta" + " subject: " + (appointment === null || appointment === void 0 ? void 0 : appointment.subject)
            + " place: " + (appointment === null || appointment === void 0 ? void 0 : appointment.place) + " description: " + (appointment === null || appointment === void 0 ? void 0 : appointment.description) + " duration: "
            + (appointment === null || appointment === void 0 ? void 0 : appointment.duration) + " beginDate: " + (appointment === null || appointment === void 0 ? void 0 : appointment.startingDate)
            + " endDate: " + (appointment === null || appointment === void 0 ? void 0 : appointment.endDate));
        return this.httpClient.post(url, appointment, this.httpOptions).pipe(operators_2.map(function (appointment) {
            _this.fixDates(appointment);
            return appointment;
        }), operators_1.tap(function (newAppointment) { return console.log("added appointent w/ id=" + newAppointment.id); }), operators_1.catchError(this.handleError('addAppointment')));
    };
    /** GET appointment by id. Will 404 if id not found */
    AppointmentService.prototype.getAppointment = function (id) {
        //TODO see this header
        var myHeader = new http_1.HttpHeaders({
            Authorization: 'Bearer JWT-token'
        });
        var url = this.baseUrl + "/" + id;
        return this.httpClient.get(url, this.httpOptions).pipe(operators_1.tap(function (_) { return console.log("fetched appointment id=" + id); }), operators_1.catchError(this.handleError("getAppointment id=" + id)));
    };
    /** GET delete appointment by id. Will 404 if id not found */
    AppointmentService.prototype.deleteAppointment = function (id) {
        //TODO see this header
        if (NaN != id) {
            console.log("en deleteAppointment del servicio: " + id);
            var myHeader = new http_1.HttpHeaders({
                Authorization: 'Bearer JWT-token'
            });
            var url = "" + (this.baseUrl + global_1.Global.deleteUrl) + id;
            return this.httpClient.get(url, this.httpOptions).pipe(operators_1.tap(function (_) { return console.log("deleted appointment id=" + id); }), operators_1.catchError(this.handleError("deleteAppointment id=" + id)));
        }
        else {
            console.log("Delete by id. ID not assigned");
            return new rxjs_1.Observable();
        }
    };
    AppointmentService.prototype.fixDates = function (appointment) {
        appointment.startingDate = Utils_1.getDateFromPiecesMonthMinusN(appointment.startingDate.toString(), 1);
        appointment.endDate = Utils_1.getDateFromPiecesMonthMinusN(appointment.endDate.toString(), 1);
        appointment.cancelDate ? Utils_1.getDateFromPiecesMonthMinusN(appointment.cancelDate.toString(), 1) : undefined;
        appointment.creationDate ? Utils_1.getDateFromPiecesMonthMinusN(appointment.creationDate.toString(), 1) : undefined;
        //console.log("starting en el map es: " + appointment.startingDate)
    };
    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    AppointmentService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // TODO: better job of transforming error for user consumption
            console.log(operation + " failed: " + error.message);
            // Let the app keep running by returning an empty result.
            return rxjs_1.of(result);
        };
    };
    AppointmentService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], AppointmentService);
    return AppointmentService;
}());
exports.AppointmentService = AppointmentService;

import {Injectable} from '@angular/core';
import {Observable, of} from "rxjs";
import {catchError, map, tap} from "rxjs/operators";
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LoginRequest} from "../../model/loginRequest";
import {environment} from "../../../environments/environment";
import {LoginResponse} from "../../model/loginResponse";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public usu: string;
  public password: string;
  headers = new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*');
  // .set('Authorization', "'Basic ' " + btoa(LoginComponent.usu + ":" + LoginComponent.password));
  httpOptions = {headers: this.headers};

  constructor(private httpClient: HttpClient) {
    this.usu = "";
    this.password = "";
  }

  setUsu(usuForm: string) {
    this.usu = usuForm;
  }

  getUsu(): string {
    return this.usu;
  }

  getPassword(): string {
    return this.password;
  }

  public getAccessFromServer(user: string, password: string): Observable<LoginResponse> {
    const url = `${environment.sLaserUrl}${environment.contextPath}${environment.loginUrl}`;
    return this.httpClient.post<LoginResponse>(url, new LoginRequest(user, password), this.httpOptions)
      .pipe(
        map(response => {
            localStorage.setItem('token', response.token);
          // console.log("returned token ", localStorage.getItem('token'));
            localStorage.setItem('userName', response.userName);
            return response;
          }, (error:any) => {console.log('en ERROR access =' + error);}
        ),
        tap(response => console.log('getting access =' + response)),
        catchError(this.handleError<LoginResponse>('getAccessFromServer'))
      );
  }

  // private handleError(error: HttpErrorResponse) {
  //   if (error.status === 0) {
  //     // A client-side or network error occurred. Handle it accordingly.
  //     console.error('An error occurred:', error.error);
  //   } else {
  //     // The backend returned an unsuccessful response code.
  //     // The response body may contain clues as to what went wrong.
  //     console.error(
  //       `Backend returned code ${error.status}, body was: `, error.error);
  //   }
  //   // Return an observable with a user-facing error message.
  //   return throwError(
  //     'Something bad happened; please try again later.');
  // }

  public isAuthenticated() {
    return !!(localStorage.getItem('token') && localStorage.getItem('userName'));
  }

  public logOut() {
    localStorage.removeItem('token');
    localStorage.removeItem('userName');
  }

  private handleError<T>(operation = 'operation') {
    this.logOut();
    return (error: any): Observable<T> => {
      console.error(`${operation} failed: ${error.status} ${error.message}`);
      return of(error as T);
    };
  }
}

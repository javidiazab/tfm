"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.EmailService = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var global_1 = require("../global");
var http_1 = require("@angular/common/http");
var operators_1 = require("rxjs/operators");
var Utils_1 = require("../../utils/Utils");
var EmailService = /** @class */ (function () {
    function EmailService(http, datePipe) {
        var _this = this;
        this.http = http;
        this.datePipe = datePipe;
        this.httpOptions = {
            headers: new http_1.HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Basic ' + btoa('admin:admin') })
        };
        /** POST send email */
        this.sendContactEmail = function (emailContactContent) {
            console.log("En servicio de envio de correo");
            var url = global_1.Global.sLaserUrl + global_1.Global.contactUrl;
            _this.emailDto.subject = "Correo de contacto de " + emailContactContent.name;
            _this.emailDto.content = _this.generateContactBody(emailContactContent);
            return _this.http.post(url, _this.emailDto, _this.httpOptions)
                .pipe(operators_1.tap(function (newResult) { return console.log("Correcto de contacto enviado correctamemte"); }), operators_1.catchError(_this.handleError('Error sending contact email')));
        };
        this.emailDto = {
            email: 'javisoftcorp@gmail.com',
            subject: 'Correo de slaser company',
            content: 'Esto es la prueba'
        };
    }
    EmailService.prototype.generateContactBody = function (emailContactContent) {
        return Utils_1.renderFile('templates/Contacto.html', emailContactContent);
    };
    EmailService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // TODO: better job of transforming error for user consumption
            console.log(operation + " failed: " + error.message);
            // Let the app keep running by returning an empty result.
            return rxjs_1.of(result);
        };
    };
    EmailService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], EmailService);
    return EmailService;
}());
exports.EmailService = EmailService;

import {Injectable} from '@angular/core';
import {Observable, of} from "rxjs";
import {EmailContactContentDto} from "../../model/EmailContactContentDto";
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, tap} from "rxjs/operators";
import {renderFile} from "../../utils/Utils";
import {emailTo} from "../../utils/Constants";
import {environment} from "../../../environments/environment";


@Injectable({
  providedIn: 'root'
})
export class EmailService {
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
    //headers: new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'Basic ' + btoa('admin:admin')})
  };
  emailDto: any;


  constructor(private http: HttpClient) {
    this.emailDto = {
      email: emailTo,
      subject: '',
      content: '',
    };
  }


  /** POST send email */
  sendContactEmail = (emailContactContent: EmailContactContentDto): Observable<boolean> => {
    const url = environment.sLaserUrl + environment.contextPath + environment.contactUrl;
    this.emailDto.subject = environment.slaserEmailSubject + " " + emailContactContent.name;
    this.emailDto.content = this.generateContactBody(emailContactContent);
    return this.http.post<boolean>(url, this.emailDto, this.httpOptions)
      .pipe(
        tap((newResult: boolean) => console.log("Contact sent successfully")),
        catchError(this.handleError<boolean>('Error sending contact email'))
      );
  }

  private generateContactBody(emailContactContent: EmailContactContentDto): string {
    return renderFile('templates/Contacto.html', emailContactContent );
  }

  private handleError<T>(operation = 'operation', result?: T) {  //TODO send toa  common place
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}



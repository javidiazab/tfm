import {daysDefinition, monthsDefinition} from "./Constants";
import * as mustache from 'mustache';
import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";
//import * as fs from "fs";


export const dateFormatDSLong = (date: Date): string => {
  return daysDefinition[date.getDay()].name + ", " + date.getDate() + " de " +
    monthsDefinition[date.getMonth()].name + " de " + date.getFullYear();
}

export const getDateFromPiecesMonthMinusN = (dateString: String, monthMinus: number): Date => {
  if (dateString) {
    let splitedDate: string[] = dateString.split(',');
    return new Date(Number(splitedDate[0]), Number(splitedDate[1]) - monthMinus, Number(splitedDate[2]), Number(splitedDate[3]),
      Number(splitedDate[4]), 0, 0);
  } else {
    return new Date(1, 1, 1);
  }
}

export const renderFile = (fileNamePath: string, values: any): string => {
  //const fileContent = fs.readFileSync(fileNamePath).toString();
  return renderContent(templateInString, values);
}

export const templateInString = "<!DOCTYPE html>\n" +
  "<html lang=\"es\">\n" +
  "\n" +
  "<head>\n" +
  "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
  "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0\" />\n" +
  "  <title>Correo de contacto desde la web</title>\n" +
  "  <style>\n" +
  "    body {\n" +
  "      color: #333333;\n" +
  "      font-family: Arial, sans-serif;\n" +
  "      font-size: 14px;\n" +
  "      line-height: 1.429;\n" +
  "    }\n" +
  "  </style>\n" +
  "</head>\n" +
  "<body>\n" +
  "<div>\n" +
  "  <header>\n" +
  "    <p>Hola Sonia,</p>\n" +
  "    <p>Tienes un correo de solicitud de información de la web:</p>\n" +
  "  </header>\n" +
  "  <section>\n" +
  "    <ul>\n" +
  "      <li>Nombre: {{name}}</li>\n" +
  "      <li>Apellidos: {{surname}}</li>\n" +
  "      <li>Teléfono: {{telephone}}</li>\n" +
  "      <li>E-mail: {{email}}</li>\n" +
  "      <li>Comentario: {{comment}}</li>\n" +
  "    </ul>\n" +
  "  </section>\n" +
  "  <footer>\n" +
  "    <p>Desde: slaserybelleza@gmail.com</p>\n" +
  "  </footer>\n" +
  "</div>\n" +
  "</body>\n" +
  "\n" +
  "</html>\n";

export const renderContent = (content: string, values: any): string => {
  return mustache.render(content, values);
}

export const handleError = (error: any): string => {
  if (error.status == 401 || error == 401 || error == 'Unauthorized') {
  return 'User not authorised';
  } else if (error.status == 403 || error == 403 || error == 'Forbidden') {
  return 'Access not allowed';
  } else if ((error.status == 423 || error == 423) && error._body) {
  return 'Information error';
  } else if (error.status == 500) {
  return "User doesn't exist";
  } else {
    return 'Error not cataloged';
  }
}

export const logout = () => {
  localStorage.removeItem("token"),
  localStorage.removeItem("userName")
}


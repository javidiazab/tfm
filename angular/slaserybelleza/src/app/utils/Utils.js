"use strict";
exports.__esModule = true;
exports.renderContent = exports.renderFile = exports.getDateFromPiecesMonthMinusN = exports.dateFormatSddmmyyyyD = exports.TimeFormatDS = exports.dateFormatDSLong = exports.dateFormatDSddmmyyyy = exports.dateFormatDS = void 0;
var date_fns_1 = require("date-fns");
var Constants_1 = require("./Constants");
var mustache = require("mustache");
var dateFormatDS = function (date) {
    return date_fns_1.format(date, 'yyyy-MM-dd');
};
exports.dateFormatDS = dateFormatDS;
var dateFormatDSddmmyyyy = function (date) {
    return date_fns_1.format(date, 'dd-MM-yyyy');
};
exports.dateFormatDSddmmyyyy = dateFormatDSddmmyyyy;
var dateFormatDSLong = function (date) {
    return Constants_1.daysDefinition[date.getDay()].name + ", " + date.getDate() + " de " +
        Constants_1.monthsDefinition[date.getMonth()].name + " de " + date.getFullYear();
};
exports.dateFormatDSLong = dateFormatDSLong;
var TimeFormatDS = function (date) {
    return date_fns_1.format(date, 'hh:mm');
};
exports.TimeFormatDS = TimeFormatDS;
var dateFormatSddmmyyyyD = function (dateddmmyyyy) {
    var splittedDate = dateddmmyyyy.split("-");
    return new Date(Number(splittedDate[2]), Number(splittedDate[1]), Number(splittedDate[0]));
};
exports.dateFormatSddmmyyyyD = dateFormatSddmmyyyyD;
var getDateFromPiecesMonthMinusN = function (dateString, monthMinus) {
    if (dateString) {
        var splitedDate = dateString.split(',');
        return new Date(Number(splitedDate[0]), Number(splitedDate[1]) - monthMinus, Number(splitedDate[2]), Number(splitedDate[3]), Number(splitedDate[4]), 0, 0);
    }
    else {
        return new Date(1, 1, 1);
    }
};
exports.getDateFromPiecesMonthMinusN = getDateFromPiecesMonthMinusN;
var renderFile = function (fileNamePath, values) {
    // const fileContent = fs.readFileSync(fileNamePath).toString();
    // return renderContent(fileContent, values);
    return "";
};
exports.renderFile = renderFile;
var renderContent = function (content, values) {
    return mustache.render(content, values);
};
exports.renderContent = renderContent;

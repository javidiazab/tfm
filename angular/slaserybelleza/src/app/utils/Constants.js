"use strict";
exports.__esModule = true;
exports.typeSelect = exports.durationSelect = exports.daysDefinition = exports.monthsDefinition = void 0;
exports.monthsDefinition = [
  {id: 1, name: "Enero"},
  {id: 2, name: "Febrero"},
  {id: 3, name: "Marzo"},
  {id: 4, name: "Abril"},
  {id: 5, name: "Mayo"},
  {id: 6, name: "Junio"},
  {id: 7, name: "Julio"},
  {id: 8, name: "Agosto"},
  {id: 9, name: "Septiembre"},
  {id: 10, name: "Octubre"},
  {id: 11, name: "Noviembre"},
  {id: 12, name: "Diciembre"}
];

exports.daysDefinition = [
  {realday: 1, badDay: 1, name: "Lunes"},
  {realday: 2, badDay: 2, name: "Martes"},
  {realday: 3, badDay: 3, name: "Miercoles"},
  {realday: 4, badDay: 4, name: "Jueves"},
  {realday: 5, badDay: 5, name: "Viernes"},
  {realday: 6, badDay: 6, name: "Sabado"},
  {realday: 7, badDay: 0, name: "Domingo"}
];

exports.calendarDays = ("Domingo", "Lunes", "Martes", "Miercoles", "Jueves",
  "Viernes", "Sabado"
]
;

exports.durationSelect = [
  {value: "0 min", selected: false},
  {value: "5 mins", selected: false},
  {value: "10 mins", selected: false},
  {value: "15 mins", selected: false},
  {value: "30 mins", selected: false},
  {value: "45 mins", selected: false},
  {value: "1 hora", selected: true},
  {value: "1,5 horas", selected: false},
  {value: "2 horas", selected: false},
  {value: "3 horas", selected: false},
  {value: "4 horas", selected: false},
  {value: "5 horas", selected: false},
  {value: "6 horas", selected: false},
  {value: "7 horas", selected: false},
  {value: "8 horas", selected: false},
  {value: "1 día", selected: false},
  {value: "2 días", selected: false},
  {value: "3 días", selected: false},
  {value: "4 días", selected: false},
  {value: "5 días", selected: false},
  {value: "1 semana", selected: false},
  {value: "2 semanas", selected: false}
];
exports.typeSelect = [
  {value: "Servicio", selected: true},
  {value: "Alquiler", selected: false},
  {value: "Formación", selected: false}
];

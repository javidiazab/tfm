export const emailTo = 'javisoftcorp@gmail.com';

export const monthsDefinition = [
  {id: 1, name: "Enero"},
  {id: 2, name: "Febrero"},
  {id: 3, name: "Marzo"},
  {id: 4, name: "Abril"},
  {id: 5, name: "Mayo"},
  {id: 6, name: "Junio"},
  {id: 7, name: "Julio"},
  {id: 8, name: "Agosto"},
  {id: 9, name: "Septiembre"},
  {id: 10, name: "Octubre"},
  {id: 11, name: "Noviembre"},
  {id: 12, name: "Diciembre"}
];

export const daysDefinition = [
  {realday: 7, badDay: 0, name: "Domingo"},
  {realday: 1, badDay: 1, name: "Lunes"},
  {realday: 2, badDay: 2, name: "Martes"},
  {realday: 3, badDay: 3, name: "Miercoles"},
  {realday: 4, badDay: 4, name: "Jueves"},
  {realday: 5, badDay: 5, name: "Viernes"},
  {realday: 6, badDay: 6, name: "Sabado"}
  ];

export const weekDays = [
  "Lunes", "Martes", "Miercoles",
  "Jueves", "Viernes", "Sabado", "Domingo"
];

export const durationSelect = [
  {value: "0 min", mins: 0},
  {value: "5 mins", mins: 5},
  {value: "10 mins", mins: 10},
  {value: "15 mins", mins: 15},
  {value: "30 mins", mins: 30},
  {value: "45 mins", mins: 45},
  {value: "1 hora", mins: 60},
  {value: "1,5 horas", mins: 80},
  {value: "2 horas", mins: 120},
  {value: "3 horas", mins: 180},
  {value: "4 horas", mins: 240},
  {value: "5 horas", mins: 300},
  {value: "6 horas", mins: 360},
  {value: "7 horas", mins: 420},
  {value: "8 horas", mins: 500},
  {value: "1 día", mins: 1440},
  {value: "2 días", mins: 2880},
  {value: "3 días", mins: 4320},
  {value: "4 días", mins: 5760},
  {value: "5 días", mins: 7200},
  {value: "1 semana", mins: 10080},
  {value: "2 semanas", mins: 20160}
];

export const typeSelect = [
  {value: "Servicio"},
  {value: "Alquiler"},
  {value: "Formacion"}
];

